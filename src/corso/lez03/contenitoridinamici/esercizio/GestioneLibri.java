package corso.lez03.contenitoridinamici.esercizio;

import java.util.ArrayList;
import java.util.Scanner;

public class GestioneLibri {

	public static void main(String[] args) {

		ArrayList<String> books = new ArrayList<String>();
		
		Scanner in = new Scanner(System.in);
		while(true) {
			System.out.println("Inserire un libro da aggiungere o 'quit' per uscire:");
			String book_name = in.nextLine();
			book_name.trim();
			if(book_name.equalsIgnoreCase("quit")) { break;}
			if(book_name.isBlank() || book_name.length() < 3) {
				System.out.println("Libro non valido! Riprova.");
				continue;
			}
			books.add(book_name);
		}
		
		System.out.println("- - Libreria: - -");
		for(int i = 0; i < books.size(); i++) {
			System.out.println(books.get(i));
		}
		
		in.close();
	}
}
