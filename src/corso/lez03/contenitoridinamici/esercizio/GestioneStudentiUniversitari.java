package corso.lez03.contenitoridinamici.esercizio;

import java.util.ArrayList;
import java.util.Scanner;

public class GestioneStudentiUniversitari {
	
	/**
	 * Creare un sistema di gestione universitaria, in particolare studenti.
	 * Il sistema deve essere in grado di:
	 * 1. Inserire un nuovo studente
	 * 2. Stampare l'elenco degli studenti gi inseriti
	 * 
	 * Ogni studente sar caratterizzato da: NOME, COGNOME, MATRICOLA, CORSO DI STUDI
	 * 
	 * Realizzare tutto in maniera interattiva con uno Scanner ed un menu di scelta funzione.
	 */

	public static void main(String[] args) {

		ArrayList<ArrayList<String>> students = new ArrayList<>();
	    
		System.out.println("Ciao, benvenuto nel nostro sistema di gestione studenti universitari!\n\nSeleziona la voce del menù che preferisci:\n");
		System.out.println("I - Inserimento nuovo studente."); // 1
		System.out.println("S - Stampa elenco studenti."); // 2
		System.out.println("E - Elimina uno studente dall'elenco."); // 3
		System.out.println("Q - Per uscire dal sistema."); // 0

	    Scanner scan = new Scanner(System.in);
		do {
			int input_code = readCommandInput(scan);
			switch(input_code) {
				case 0: 
					System.out.println("\nTerminato");
					scan.close();
					System.exit(0);
				case 1:
					ArrayList<String> new_student = insertNewStudent(scan);
					if(new_student != null) {
						students.add(new_student);
						System.out.println("Studente inserito con successo!");
					}
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
				case 2:
					printStudents(students);
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
				case 3:
					int index = deleteStudent(scan, students);
					if(index == -1) { System.out.println("Lo studente che vuoi eliminare non è presente nell'elenco.");}
					else { 
						students.remove(index);
						System.out.println("Studente eliminato con successo!");
					}
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
			}
		}
		while(true);
	}
	
	public static void printStudents(ArrayList<ArrayList<String>> students) {
		
		System.out.println("- - - Elenco Studenti: - - -");
	    for(int i = 0; i < students.size(); i++) {
	    	for(int j = 0; j < students.get(i).size(); j++) {
	    		System.out.print(students.get(i).get(j));
	    		if(j == students.get(i).size()-1) { System.out.print("\n");}
	    		else { System.out.print(" - ");}
	    	}
	    }
	    System.out.print("\n");
	}
	
	public static int readCommandInput(Scanner scan) {
		
		while(true) {
			String answer = scan.nextLine();
			answer.trim();
			if(answer.isBlank() || answer.length() != 1 ) {
				System.out.println("Input non valido! Riprova.");
			}
			else if(answer.equalsIgnoreCase("I")) { return 1;}
			else if(answer.equalsIgnoreCase("S")) { return 2;}
			else if(answer.equalsIgnoreCase("E")) { return 3;}
			else if(answer.equalsIgnoreCase("Q")) { return 0;}
			else {
				System.out.println("Comando non valido! Riprova.");
			}
		}
	}
	
	public static ArrayList<String> insertNewStudent(Scanner scan) {
		
		ArrayList<String> new_student = new ArrayList<String>();
		
		System.out.println("Inserisci il nome dello studente:");
		String answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Nome non valido! Inserimento interrotto.");
			return null;
		}
		new_student.add(answer);
		
		System.out.println("Inserisci il cognome dello studente:");
		answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Cognome non valido! Inserimento interrotto.");
			return null;
		}
		new_student.add(answer);
		
		System.out.println("Inserisci la matricola dello studente:");
		answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Matricola non valida! Inserimento interrotto.");
			return null;
		}
		new_student.add(answer);
		
		System.out.println("Inserisci il corso di studi dello studente:");
		answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Corso di studi non valido! Inserimento interrotto.");
			return null;
		}
		new_student.add(answer);
		
		return new_student;
	}
	
	public static int deleteStudent(Scanner scan, ArrayList<ArrayList<String>> library) {
		
		System.out.println("Inserisci il NOME dello studente che vuoi eliminare:");
		String name_to_erase = scan.nextLine();
		name_to_erase.trim();
		
		System.out.println("Inserisci il COGNOME dello studente che vuoi eliminare:");
		String surname_to_erase = scan.nextLine();
		surname_to_erase.trim();		
		
		int index_to_erase = -1;
		
		for(int i = 0; i < library.size(); i++) {
			if(library.get(i).get(0).equalsIgnoreCase(name_to_erase)) {
				if(library.get(i).get(1).equalsIgnoreCase(surname_to_erase)) {
					index_to_erase = i;
					break;
				}
			}
		}
		
		return index_to_erase;
	}

}
