package corso.lez03.contenitoridinamici;

import java.util.ArrayList;
import java.util.Scanner;

public class ContenitoriDinamici {

	public static void main(String[] args) {

		ArrayList<String> rubrica = new ArrayList<String>();
		
		Scanner in = new Scanner(System.in);
		while(true) {
			System.out.println("Inserire un nome da aggiungere in rubrica o 'quit' per uscire:");
			String name = in.nextLine();
			name.trim();
			if(name.equalsIgnoreCase("quit")) { break;}
			if(name.isBlank() || name.length() < 3) {
				System.out.println("Nome non valido! Riprova.");
				continue;
			}
			rubrica.add(name);
		}
		
		System.out.println("Rubrica: " + rubrica);
		
		in.close();
	}

}
