package corso.lez03.contenitoridinamici;

import java.util.ArrayList;
import java.util.Scanner;

public class ContenitoriDinamiciComplessi {

	public static void main(String[] args) {
		
		ArrayList<String> libro_1 = new ArrayList<String>();
		libro_1.add("Storia della buonanotte");
		libro_1.add("Favilli");
		libro_1.add("12345-12345-12345");
		
		ArrayList<String> libro_2 = new ArrayList<String>();
		libro_2.add("Nessuno scrive al Federale");
		libro_2.add("Vitali");
		libro_2.add("432434-43423-43243");
		
		ArrayList<String> libro_3 = new ArrayList<String>();
		libro_3.add("Buonvino e il caso del bambino scomparso");
		libro_3.add("Veltroni");
		libro_3.add("76575-765767-765765");
		
		ArrayList<String> libro_4 = new ArrayList<String>();
		libro_4.add("Storia del Buongiorno");
		libro_4.add("Favilli");
		libro_4.add("12345-12345-12645");
		
		ArrayList<String> libro_5 = new ArrayList<String>();
		libro_5.add("Il volo del corvo a Roma");
		libro_5.add("Veltroni");
		libro_5.add("76575-765867-765765");
		
		ArrayList<String> libro_6 = new ArrayList<String>();
		libro_6.add("Il volo del gabbiano sul tevere");
		libro_6.add("Veltroni");
		libro_6.add("76575-765867-765165");
		
		ArrayList<ArrayList<String>> library = new ArrayList<>();
		library.add(libro_1);
		library.add(libro_2);
		library.add(libro_3);
		library.add(libro_4);
		library.add(libro_5);
		library.add(libro_6);
	    
		System.out.println("Ciao, benvenuto nel nostro sistema di gestione libri!\n\nSeleziona la voce del menù che preferisci:\n");
		System.out.println("I - Inserimento nuovo libro.\n"); // 1
		System.out.println("S - Stampa elenco libri.\n"); // 2
		System.out.println("E - Elimina un libro in elenco.\n"); // 3
		System.out.println("Q - Per uscire dal sistema.\n"); // 0

	    Scanner scan = new Scanner(System.in);
		do {
			int input_code = readCommandInput(scan);
			switch(input_code) {
				case 0: 
					System.out.println("Termino.");
					System.exit(0);
				case 1:
					ArrayList<String> new_book = insertNewBook(scan);
					if(new_book != null) {
						library.add(new_book);
						System.out.println("Libro inserito con successo!");
					}
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
				case 2:
					printLibrary(library);
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
				case 3:
					int index = deleteBook(scan, library);
					if(index == -1) { System.out.println("Il libro che vuoi eliminare non è presente nella libreria.");}
					else { 
						library.remove(index);
						System.out.println("Libro eliminato con successo!");
					}
					System.out.println("\nCosa vuoi fare adesso?");
					continue;
			}
		}
		while(true);
	}
	
	public static void printLibrary(ArrayList<ArrayList<String>> library) {
		
		System.out.println("- - - Liberia: - - -");
	    for(int i = 0; i < library.size(); i++) {
	    	for(int j = 0; j < library.get(i).size(); j++) {
	    		System.out.print(library.get(i).get(j));
	    		if(j == library.get(i).size()-1) { System.out.print("\n");}
	    		else { System.out.print(" - ");}
	    	}
	    }
	    System.out.print("\n");
	}
	
	public static int readCommandInput(Scanner scan) {
		
		while(true) {
			String answer = scan.nextLine();
			answer.trim();
			if(answer.isBlank() || answer.length() != 1 ) {
				System.out.println("Input non valido! Riprova.");
			}
			else if(answer.equalsIgnoreCase("I")) { return 1;}
			else if(answer.equalsIgnoreCase("S")) { return 2;}
			else if(answer.equalsIgnoreCase("E")) { return 3;}
			else if(answer.equalsIgnoreCase("Q")) { return 0;}
			else {
				System.out.println("Comando non trovato! Riprova.");
			}
		}
	}
	
	public static ArrayList<String> insertNewBook(Scanner scan) {
		
		ArrayList<String> new_book = new ArrayList<String>();
		
		System.out.println("Inserisci il titolo del libro:");
		String answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Titolo non valido! Inserimento interrotto.");
			return null;
		}
		new_book.add(answer);
		
		System.out.println("Inserisci l'autore del libro:");
		answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("Autore non valido! Inserimento interrotto.");
			return null;
		}
		new_book.add(answer);
		
		System.out.println("Inserisci l'ISBN del libro:");
		answer = scan.nextLine();
		answer.trim();
		if(answer.isBlank() || answer.length() < 3 ) {
			System.out.println("ISBN non valido! Inserimento interrotto.");
			return null;
		}
		new_book.add(answer);
		
		return new_book;
	}
	
	public static int deleteBook(Scanner scan, ArrayList<ArrayList<String>> library) {
		
		System.out.println("Inserisci il titolo del libro che vuoi eliminare:");
		String book_to_erase = scan.nextLine();
		book_to_erase.trim();
		int index_to_erase = -1;
		
		for(int i = 0; i < library.size(); i++) {
			if(library.get(i).get(0).equalsIgnoreCase(book_to_erase)) {
				index_to_erase = i;
				break;
			}
		}
		
		return index_to_erase;
	}
	
	

}
