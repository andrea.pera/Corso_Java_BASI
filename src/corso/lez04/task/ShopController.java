package corso.lez04.task;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Controllore che gestisce le operazione effettuate da interfaccia sul modello dei dati.
 */
public class ShopController {
	
    private static ShopController instance = null;
    
	private static Inventory inventory = null; // Inventario-Magazzino.
	private static ModelsRegistry models_registry = null; // Registro dei Modelli dei Prodotti.
	private static CashRegistry cash_registry = null; // Registro di Cassa (vendite).
 
    private ShopController() {
    	inventory = new Inventory();
    	models_registry = new ModelsRegistry();
    	cash_registry = new CashRegistry();
    } 
 
    public static ShopController getInstance() {
        if (instance == null) {
            instance = new ShopController();
        }
        return instance;
    }
    
    /**
     * Inserisce in magazzino una nuova scarpa.
     */
    public Shoe addShoe(String brand, 
    					long code,
    					int size,
    					String material, 
    					String color,
    					String type,
    					int quantity,
    					double price_value,
    					String price_currency,
    					boolean isAvailable,
    					int percentage, 
    					String model,
    					String producer) 
    {
    	Shoe new_shoe = new Shoe(brand, code, size, material, color, type, quantity);
    	new_shoe.setPrice(new Price(price_value, price_currency));
    	new_shoe.setDiscount(new Discount(isAvailable, percentage));
    	new_shoe.setModelInfo(models_registry.generateModelInfo(model, producer, new_shoe.getQuantity()));
    	inventory.addShoe(new_shoe);
		System.out.println("\nINFO: Nuova scarpa inserita con successo in magazzino! Codice " + new_shoe.getCode() + "\n");
		
    	return new_shoe;
    }
    
    /**
     * Rimuove dal magazzino una scarpa specificata.
     */
    public void removeShoe(Shoe shoe) {
    	
		if (inventory.removeShoe(shoe)) {
			System.out.println("\nINFO: La scarpa con codice '" + shoe.getCode() + "' è stata rimossa dal magazzino!\n");
		}
		else {
			System.out.println("\nWARNING: La scarpa specificata non è presente nel magazzino!\n");
		}
	}
    
    /*
     * Rimuove dal magazzino la scarpa identificata tramite codice.
     */
    public void removeShoeByCode(int shoe_code) {
    	
    	int result = inventory.removeShoeByCode(shoe_code);
		if(result != -1) {
			System.out.println("\nINFO: La scarpa con codice '" + shoe_code + "' è stato rimosso dal magazzino!\n");
		}
		else {
			System.out.println("\nWARNING: La scarpa con codice '" + shoe_code + "' non è presente nel magazzino!\n");
		}
    }
    
    /*
     * Memorizza la vendita di una scarpa identificata tramite codice.
     */
    public void sellShoeByCode(int shoe_code, int quantity, LocalDate date) {
    	
		Shoe sale_shoe = inventory.getShoeByCode(shoe_code);
		if(sale_shoe == null) {
			System.out.println("\nWARNING: La scarpa con codice '" + shoe_code + "' non è presente nel magazzino!\n");
		}
		else {
			sale_shoe.setQuantity(sale_shoe.getQuantity() - quantity);
			double discount_value;
			double price;
			Discount discount = sale_shoe.getDiscount();
			if(discount.getAvailability()) {
				discount_value = sale_shoe.getPrice().discountValue(discount);
				price = sale_shoe.getPrice().discountedPrice(discount);
			}
			else {
				discount_value = 0.0d;
				price = sale_shoe.getPrice().getValue();
			}
					
			cash_registry.addSale(sale_shoe.getModelInfo(), quantity, date, price, discount_value);
		}
    }
    
    /**
     * Inserisce in magazzino una nuova Cintura.
     */
    public Belt addBelt(String brand, 
    					long code,
    					int size,
    					String material, 
    					String color,
    					String type,
    					int quantity,
    					double price_value,
    					String price_currency,
    					boolean isAvailable,
    					int percentage, 
    					String model,
    					String producer) 
    {
    	Belt new_belt = new Belt(brand, code, size, material, color, type, quantity);
    	new_belt.setPrice(new Price(price_value, price_currency));
    	new_belt.setDiscount(new Discount(isAvailable, percentage));
    	new_belt.setModelInfo(models_registry.generateModelInfo(model, producer, new_belt.getQuantity()));
    	inventory.addBelt(new_belt);
		System.out.println("\nINFO: Nuova cintura inserita con successo in magazzino! Codice " + new_belt.getCode() + "\n");
		
    	return new_belt;
    }
    
    /*
     * Memorizza la vendita di una cintura identificata tramite codice.
     */
    public void sellBeltByCode(int belt_code, int quantity, LocalDate date) {
    	
		Belt sale_belt = inventory.getBeltByCode(belt_code);
		if(sale_belt == null) {
			System.out.println("\nWARNING: La cintura con codice '" + belt_code + "' non è presente nel magazzino!\n");
		}
		else {
			sale_belt.setQuantity(sale_belt.getQuantity() - quantity);
			double discount_value;
			double price;
			Discount discount = sale_belt.getDiscount();
			if(discount.getAvailability()) {
				discount_value = sale_belt.getPrice().discountValue(discount);
				price = sale_belt.getPrice().discountedPrice(discount);
			}
			else {
				discount_value = 0.0d;
				price = sale_belt.getPrice().getValue();
			}
			
			cash_registry.addSale(sale_belt.getModelInfo(), quantity, date, price, discount_value);
		}
    }
    
    public int calculateShoesQuantity() {
    	return Product.calculateQuantity(new ArrayList<Product>(inventory.getShoes()));
    }
    
    public int calculateFemaleShoesQuantity() {
    	return Product.calculateFemaleQuantity(new ArrayList<Product>(inventory.getShoes()));   
    }
    
    public int calculateMaleShoesQuantity() {
    	return Product.calculateMaleQuantity(new ArrayList<Product>(inventory.getShoes()));   
    }
    
    public int calculateBeltsQuantity() {
    	return Product.calculateQuantity(new ArrayList<Product>(inventory.getBelts()));
    }
    
    public int calculateFemaleBeltsQuantity() {
    	return Product.calculateFemaleQuantity(new ArrayList<Product>(inventory.getBelts()));   
    }
    
    public int calculateMaleBeltsQuantity() {
    	return Product.calculateMaleQuantity(new ArrayList<Product>(inventory.getBelts()));   
    }
    
    public void calculateEarnedMoneyandDiscount() {
    	cash_registry.calculateEarnedMoneyandDiscount();
    }
    
    public void printModelsList() {
    	models_registry.printModelsList();
    }
    
    public void printQuantitiesList() {
    	models_registry.printQuantitiesList();
    }
    
    public void printAllShoes() {
    	inventory.printAllShoes();
    }
    
    public void printAllBelts() {
    	inventory.printAllBelts();
    }
    
    public void printSalesMap() {
    	cash_registry.printSalesMap();
    }
    
    public void printDatesMaps() {
    	cash_registry.printDatesMaps();
    }
    
    public void printReportSinceSpecifiedDate(LocalDate starting_Date) {
    	cash_registry.printReportSinceSpecifiedDate(starting_Date);
    }
}
