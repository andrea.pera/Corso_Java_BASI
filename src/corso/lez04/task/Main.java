package corso.lez04.task;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Main {
	
	/*Creare un sistema di gestione prodotti di un E-Commerce, in particolare scarpe. Il task può essere suddiviso in questo modo:

		Creare le classi per memorizzare i prodotti caratterizzate (almeno) dai seguenti attributi:
		Marca
		Codice
		Taglia
		Tessuto
		Colore
		Genere (maschile, femminile, unisex)
		Quantità disponibile
		Creare quattro scarpe ed inserirle all'interno di un contenitore in grado di ospitare un elenco di prodotti.

		Stampare il dettaglio delle scarpe in maniera automatica.

		HARD: Creando una decina di prodotti (di diverso Genere)
		---- 4a. contare quanti prodotti in totale ho in magazzino.
		---- 4b. Contare quanti prodotti ho del solo genere femminile.

		HARD: Come faccio ad eliminare una scarpa tramite il codice?
		
		- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		PROJECT_ID: NEW SHOES SELLER 2021
		PROJECT_GROUP: SHOESELLER_SPA 
		
		Funzionalità di base
		
		1) Il sistema deve permettere l'inserimento e la rimozione di elementi dall'inventario (Test: OK, Collaudo: OK, rilasciato il 20/4/2021)
		
		NUOVE FUNZIONALITA
		
		2) Il sistema deve tenere traccia della quantità di scarpe per ogni modello in inventario.
			2.1) Il sistema deve tenere traccia della quantità di cinture ed altri optional.
			
		3) Il sistema deve tenere traccia delle vendite e del relativo prezzo tramite un registro di cassa.
		
		4) Il sistema deve garantire l'univocità di un modello in inventario tramite un codice alfanumerico 
		   generato automaticamente seguendo la formula <PRODUTTORE>-<MODELLO>-<NUMERALE>
		
		5) Il sistema deve generare un report di vendita degli ultimi 3 mesi di attività del negozio.
	*/

	public static void main(String[] args) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
		
		ShopController shop_controller = ShopController.getInstance();
		
		Shoe shoe1 = shop_controller.addShoe("Armani", 1000, 44, "Pelle", "Rosso", "Unisex", 1, 50.99, "Euro", true, 40, "Scarpa_Base", "XXX");
		Shoe shoe2 = shop_controller.addShoe("Dolce&Gabbana", 1001, 40, "Plastica", "Blu", "Maschile", 2, 30.59, "Euro", false, 0, "Scarpa_Pro", "YYY");
		Shoe shoe3 = shop_controller.addShoe("Prada", 1002, 39, "Pelle", "Celeste", "Femminile", 20, 80.99, "Euro", true, 20, "Scarpa_Custom", "ZZZ");
		Shoe shoe4 = shop_controller.addShoe("Prada", 1003, 37, "Licra", "Verde", "Unisex", 3, 40.99, "Euro", true, 15, "Scarpa_Special", "AAA");
		Shoe shoe5 = shop_controller.addShoe("Armani", 1004, 42, "Pelle", "Giallo", "Unisex", 6, 50.99, "Euro", true, 50, "Scarpa_Base", "XXX");
		
		Belt belt1 = shop_controller.addBelt("Dolce&Gabbana", 1005, 120, "Pelle", "Nero", "Maschile", 20, 20.19, "Euro", false, 0, "Cintura_Base", "YYY");
		Belt belt2 = shop_controller.addBelt("Armani", 1006, 130, "Pelle", "Nero", "Maschile", 30, 30.29, "Euro", false, 0, "Cintura_Base", "XXX");
		/*
		System.out.print("Lista Modelli: ");
		shop_controller.printModelsList();
		System.out.print("Lista Quantità Modelli: ");
		shop_controller.printQuantitiesList();
		System.out.print("\n- - - - - - - - - - - - - Lista Scarpe: - - - - - - - - - - - - \n");
		shop_controller.printAllShoes();	
		System.out.print("\n- - - - - - - - - - - - - Lista Cinture: - - - - - - - - - - - - \n");
		shop_controller.printAllBelts();	
		*/
		/*
		System.out.println("Quantità Scarpe in Magazzino: " + shop_controller.calculateShoesQuantity());
		System.out.println("Quantità Scarpe 'Femminili' in Magazzino: " + shop_controller.calculateFemaleShoesQuantity());
		System.out.println("Quantità Scarpe 'Maschili' in Magazzino: " + shop_controller.calculateMaleShoesQuantity());

		System.out.println("Quantità Cinture in Magazzino: " + shop_controller.calculateBeltsQuantity());
		System.out.println("Quantità Cinture 'Femminili' in Magazzino: " + shop_controller.calculateFemaleBeltsQuantity());
		System.out.println("Quantità Cinture 'Maschili' in Magazzino: " + shop_controller.calculateMaleBeltsQuantity());
		*/
		/*
		// Rimozione tramite CODICE:
		shop_controller.removeShoeByCode(1002);
		// Rimozione tramite OGGETTO:
		shop_controller.removeShoe(shoe1);
		System.out.print("\n- - - - - - - - - - - Lista Scarpe: - - - - - - - - - - \n");
		shop_controller.printAllShoes();		
		*/
		
		// Vendo 4 scarpe con codice 1002:
		shop_controller.sellShoeByCode(1002, 4, LocalDate.of(2019, 04, 20));
		
		// Vendo 1 scarpe con codice 1003:
		shop_controller.sellShoeByCode(1003, 1, LocalDate.of(2019, 06, 13));
		
		// Vendo 2 scarpe con codice 1000:
		shop_controller.sellShoeByCode(1000, 2, LocalDate.of(2020, 11, 19));
		
		// Vendo 4 cinture con codice 1006:
		shop_controller.sellBeltByCode(1006, 4, LocalDate.of(2020, 12, 01));
		
		// Vendo 2 scarpe con codice 1002:
		shop_controller.sellShoeByCode(1002, 2, LocalDate.of(2021, 01, 05));
		
		// Vendo 5 scarpe con codice 1002:
		shop_controller.sellShoeByCode(1002, 5, LocalDate.of(2021, 01, 05));
		
		// Vendo 1 scarpe con codice 1003:
		shop_controller.sellShoeByCode(1003, 1, LocalDate.of(2021, 04, 20));
		
		// Vendo 2 cinture con codice 1005:
		shop_controller.sellBeltByCode(1005, 2, LocalDate.of(2021, 04, 20));
		
		// Vendo 1 scarpe con codice 1000:
		shop_controller.sellShoeByCode(1000, 1, LocalDate.of(2021, 04, 20));
		
		System.out.print("\nRegistro di Cassa: ");
		shop_controller.printSalesMap();
		
		System.out.print("\nLista Modelli: ");
		shop_controller.printModelsList();
		System.out.print("Lista Quantità Modelli: ");
		shop_controller.printQuantitiesList();
		
		System.out.print("\n - - - - - - - - - - - Report di Vendite TOTALI: - - - - - - - - - - - - \n");
		shop_controller.printDatesMaps();
		
		LocalDate starting_Date = LocalDate.of(2019, 01, 01);
		System.out.print("\n - - - - - - - - - - Statistiche Vendite DALLA DATA: " + starting_Date.format(formatter) + " - - - - - - - - - - - \n");
		shop_controller.printReportSinceSpecifiedDate(starting_Date);
		
		shop_controller.calculateEarnedMoneyandDiscount();
	}

}
