package corso.lez04.task;

/**
 * Prezzo associato ad un prodotto.
 */
public class Price { // Prezzo
	
	private double value; // Prezzo
	private String currency; // Valuta
	
	public Price() {}
	
	public Price(double value, String currency) {
		this.setValue(value);
		this.setCurrency(currency);
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * Resituisce il prezzo scontato:
	 */
	public double discountedPrice(Discount discount) {
		
		if(!discount.getAvailability()) { return -1;} // Sconto NON applicabile.
		else {
			// Prezzo Scontato:
			// 	  Prezzo Base - Sconto.
			return this.value - (this.value * discount.getPercentage()) / 100;
		}
	}
	
	/**
	 * Restituisce il valore dello sconto:
	 */
	public double discountValue(Discount discount) {
		if(!discount.getAvailability()) { return -1;}
		else { return (this.value * discount.getPercentage()) / 100;}
	}
}
