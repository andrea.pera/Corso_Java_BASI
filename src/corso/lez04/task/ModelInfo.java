package corso.lez04.task;

/**
 * Informazioni sul modello di un prodotto.
 */
public class ModelInfo {
	
	private String model;
	private String producer;
	private long id; // Numerale.
	private int quantity; // Quantita di prodotto RIMANENTE per un modello.
	private String alphanumerical_code; // = <PRODUTTORE>-<MODELLO>-<NUMERALE>
	
	public ModelInfo() {}
	
	public ModelInfo(String model, String producer) {
		this.setModel(model);
		this.setProducer(producer);
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public long getId() {
		return id;
	}

	public void setId(long counter) {
		this.id = counter;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getAlphanumericalCode() {
		return alphanumerical_code;
	}

	public void GenerateAlphanumericalCode() {
		this.alphanumerical_code = this.producer + "-" + this.model + "-" + this.id;
	}
}
