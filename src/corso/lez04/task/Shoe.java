package corso.lez04.task;

import java.util.ArrayList;

/**
 * Rappresentazione Software di "Scarpa".
 */
public class Shoe extends Product {
	
	public Shoe() {
		super();
	}
	
	public Shoe(String brand, long code, int size, String material, String color, String type, int quantity) {
		super(brand, code, size, material, color, type, quantity);
	}
	
	public Shoe(String brand, long code, int size, String material, String color, String type, int quantity, Price price, Discount discount, ModelInfo model_info) {
		super(brand, code, size, material, color, type, quantity, price, discount, model_info);
	}
}
