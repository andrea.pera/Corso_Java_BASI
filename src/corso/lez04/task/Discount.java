package corso.lez04.task;

/**
 * Sconto associato ad un prodotto.
 */
public class Discount {

	private boolean isAvailable; // Applicabilità dello sconto.
	private int percentage; // Percentuale dello sconto.
	
	public Discount() {}
	
	public Discount(boolean isAvailable, int percentage) {
		this.setAvailability(isAvailable);
		this.setPercentage(percentage);
	}
	
	public boolean getAvailability() {
		return isAvailable;
	}

	public void setAvailability(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}
}