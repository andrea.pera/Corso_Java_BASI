package corso.lez04.task;

/**
 * Rappresentazione software di "Cintura".
 */
public class Belt extends Product {
		
	public Belt() {
		super();
	}

	public Belt(String brand, long code, int size, String material, String color, String type, int quantity) {
		super(brand, code, size, material, color, type, quantity);
	}
	
	public Belt(String brand, long code, int size, String material, String color, String type, int quantity, Price price, Discount discount, ModelInfo model_info) {
		super(brand, code, size, material, color, type, quantity, price, discount, model_info);
	}
}
