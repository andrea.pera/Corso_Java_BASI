package corso.lez04.task;

import java.util.ArrayList;

public abstract class Product {
	
	protected String brand; // MARCA
	protected long code;
	protected int size; // TAGLIA
	protected String material; // TESSUTO
	protected String color;
	protected String type; // GENERE: Maschile, Femminile, Unisex
	protected int quantity; // Quantita disponibile
	protected Price price;
	protected Discount discount;
	protected ModelInfo model_info;
	
	public Product() {}
	
	public Product(String brand, long code, int size, String material, String color, String type, int quantity) {
		this.setBrand(brand);
		this.setCode(code);
		this.setSize(size);
		this.setMaterial(material);
		this.setColor(color);
		this.setType(type);
		this.setQuantity(quantity);
	}
	
	public Product(String brand, long code, int size, String material, String color, String type, int quantity, Price price, Discount discount, ModelInfo model_info) {
		this.setBrand(brand);
		this.setCode(code);
		this.setSize(size);
		this.setMaterial(material);
		this.setColor(color);
		this.setType(type);
		this.setQuantity(quantity);
		this.setPrice(price);
		this.setDiscount(discount);
		this.setModelInfo(model_info);
	}
	
	public ModelInfo getModelInfo() {
		return model_info;
	}
	public void setModelInfo(ModelInfo model_info) {
		this.model_info = model_info;
	}
	public Discount getDiscount() {
		return discount;
	}
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}
	public Price getPrice() {
		return price;
	}
	public void setPrice(Price price) {
		this.price = price;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public long getCode() {
		return code;
	}
	public void setCode(long code) {
		this.code = code;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((Product) obj).code == this.code;
	}
	
	public static int calculateQuantity(ArrayList<Product> product_list) {
		
		int count = 0;
		for(int i = 0; i < product_list.size(); i++) {
			count += product_list.get(i).getQuantity();
		}
		return count;
	}
	
	public static int calculateFemaleQuantity(ArrayList<Product> product_list) {
		
		int count = 0;
		for(int i = 0; i < product_list.size(); i++) {
			if (product_list.get(i).getType().equals("Femminile")) {
				count += product_list.get(i).getQuantity();
			}
		}
		return count;
	}
	
	public static int calculateMaleQuantity(ArrayList<Product> product_list) {
		
		int count = 0;
		for(int i = 0; i < product_list.size(); i++) {
			if (product_list.get(i).getType().equals("Maschile")) {
				count += product_list.get(i).getQuantity();
			}
		}
		return count;
	}
	
	public void printProduct() {
		
		String discount;
		
		double discountPrice = this.getPrice().discountedPrice(this.getDiscount());
		if(discountPrice != -1) {
			discount = Double.toString(discountPrice);
		}
		else {
			discount = "Sconto non applicabile";
		}
		
		System.out.println("Marca: " + this.getBrand() + 
						   "\nCodice: " + this.getCode() +
						   "\nTaglia: " + this.getSize() + 
						   "\nTessuto: " + this.getMaterial() + 
						   "\ncolore: " + this.getColor() + 
						   "\nGenere: " + this.getType() + 
						   "\nQuantita: " + this.getQuantity() +
						   "\nPrezzo: " + this.price.getValue() + " " + this.getPrice().getCurrency() + 
						   "\nPrezzo Scontato: " + discount + 
						   "\nModello: " + this.getModelInfo().getModel() + 
						   "\nProduttore: " + this.getModelInfo().getProducer() +
						   "\nCodice Alfanumerico: " + this.getModelInfo().getAlphanumericalCode() +
						   "\n"
				);
	}
}
