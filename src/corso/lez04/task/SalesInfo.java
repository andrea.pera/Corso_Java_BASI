package corso.lez04.task;

public class SalesInfo {
	
	private double total_money; // Somma dei prezzi dei prodotti venduti (eventualmente scontati).
	private double total_discount_value; // Somma degli sconti applicati ai prodotti venduti.
	private int total_quantity; // Somma della quantita dei prodotti venduti.
	
	public SalesInfo() {}
	
	public SalesInfo(double price, double discount_value, int quantity) {
		this.setTotalMoney(price * quantity);
		this.setTotalDiscountValue(discount_value * quantity);;
		this.setTotalQuantity(quantity);
	}

	public double getTotalMoney() {
		return total_money;
	}

	public void setTotalMoney(double total_price) {
		this.total_money = total_price;
	}

	public double getTotalDiscountValue() {
		return total_discount_value;
	}

	public void setTotalDiscountValue(double total_discount_value) {
		this.total_discount_value = total_discount_value;
	}

	public int getTotalQuantity() {
		return total_quantity;
	}

	public void setTotalQuantity(int total_quantity) {
		this.total_quantity = total_quantity;
	}
	
	/**
	 * AGGIORNA le informazioni di vendita di un modello aggiungendo quelle della nuova vendita:
	 * @param new_price: Prezzo (eventualmente scontato) del modello venduto.
	 * @param new_discounted_value: Sconto applicato al modello venduto.
	 * @param new_quantity: Quantita venduta di quel modello.
	 */
	public void updateSalesInfo(double new_price, double new_discount_value, int new_quantity) {
		this.total_money += (new_price * new_quantity);
		this.total_discount_value += (new_discount_value * new_quantity);
		this.total_quantity += new_quantity;
	}
	
	@Override
	public String toString() {
		return "SOLDI Complessivi delle Vendite = " + this.total_money +
			   ", SCONTO Complessivo = " + this.total_discount_value +
			   ", QUANTITA Venduta = " + this.total_quantity;
	}
}
