package corso.lez04.task;

import java.util.ArrayList;

/**
 * Inventario/Magazzino dei prodotti disponibili.
 */
public class Inventory {
	
	private static ArrayList<Shoe> shoes = null;
	private static ArrayList<Belt> belts = null;
	
	public Inventory() {
		shoes = new ArrayList<Shoe>();
		belts = new ArrayList<Belt>();
	}

	public ArrayList<Shoe> getShoes() {
		return shoes;
	}

	public void setShoes(ArrayList<Shoe> shoes) {
		shoes.addAll(shoes);
	}
	
	public Shoe getShoeByCode(int code) {
		for(Shoe shoe: shoes) {
			if(shoe.getCode() == code) {
				return shoe;
			}
		}
		return null;
	}
	
	public void addShoe(Shoe shoe) {
		shoes.add(shoe);
	}
	
	public boolean removeShoe(Shoe shoe) {
		return shoes.remove(shoe);
	}
	
	public int removeShoeByCode(int shoe_code) {
		
		int index_to_remove = -1;
		
		for(int i = 0; i < shoes.size(); i++) {
			if(shoes.get(i).getCode() == shoe_code) {
				index_to_remove = i;
				break;
			}
		}
		
		if(index_to_remove != -1) {
			shoes.remove(index_to_remove);
		}
		
		return index_to_remove;
	}
	
	public void printAllShoes() {
		for(int i = 0; i < shoes.size(); i++) {
			shoes.get(i).printProduct();
		}
	}
	
	public ArrayList<Belt> getBelts() {
		return belts;
	}
	
	public void addBelt(Belt belt) {
		belts.add(belt);
	}
	
	public void printAllBelts() {
		for(int i = 0; i < belts.size(); i++) {
			belts.get(i).printProduct();
		}
	}
	
	public Belt getBeltByCode(int code) {
		for(Belt belt: belts) {
			if(belt.getCode() == code) {
				return belt;
			}
		}
		return null;
	}
	
}
