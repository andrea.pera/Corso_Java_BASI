package corso.lez04.task;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Registro di Cassa: Memorizza le vendite e le date per ogni modello dei prodotti.
 */
public class CashRegistry {
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
	
	/* 
	 * HashMap Numero di Vendite:
	 * - CHIAVE: Codice alfanumerico del modello del prodotto.
	 * - VALORE: Numero di prodotti venduti di quel modello.
	 * */
	HashMap<String, Integer> sales_map = null; // Mappa di vendite
	
	/*
	 * HashMap Date delle Vendite:
	 * - CHIAVE: Data (stringa formattata) del giorno di vendita di un prodotto.
	 * - VALORE: HashMap contenente le INFO di vendita di quel prodotto per quella data.
	 */
	HashMap<String, HashMap<String, SalesInfo>> dates_map = null;
	
	public CashRegistry() {
		this.sales_map = new HashMap<>();
		this.dates_map = new HashMap<>();
	}
	
	/**
	 * Memorizza la vendita di un Prodotto.
	 */
	public void addSale(ModelInfo model_info, int quantity, LocalDate sale_date, double price, double discount_value) {
		
		// MEMORIZZO LE INFORMAZIONI NELLA HashMap DELLE VENDITE:
		if(this.sales_map.containsKey(model_info.getAlphanumericalCode())) {
			// -> Ho gia venduto dei prodotti di quello stesso modello.
			this.sales_map.replace(model_info.getAlphanumericalCode(), this.sales_map.get(model_info.getAlphanumericalCode()) + quantity); // AGGIORNO la quantita venduta di quel modello.
		}
		else { 
			// -> NON avevo ancora venduto dei prodotti di quello stesso modello.
			this.sales_map.put(model_info.getAlphanumericalCode(), quantity); // INSERISCO la quantita venduta di quel modello.
		}
		model_info.setQuantity(model_info.getQuantity() - quantity); // Aggiorno la quantita rimanente di quel modello.
		
		
		// MEMORIZZO LE INFORMAZIONI NELLA HashMap DELLE DATE:
		HashMap<String, SalesInfo> temp_map = null;
		SalesInfo sale_Info = null;
		if(this.dates_map.containsKey(sale_date.format(this.formatter))) { // Cerco una Entry per quella data.
			// -> Ho gia venduto dei prodotti in quella data.
			temp_map = this.dates_map.get(sale_date.format(this.formatter)); // Prendo la HashMap interna con le Entry per le vendite in quella specifica data.
			if(temp_map.containsKey(model_info.getAlphanumericalCode())) { // Cerco la Entry delle vendite per quello stesso modello.
				sale_Info = temp_map.get(model_info.getAlphanumericalCode());
				sale_Info.updateSalesInfo(price, discount_value, quantity);
				temp_map.replace(model_info.getAlphanumericalCode(), sale_Info); // AGGIORNO la quantita venduta di quel modello in quella data.
			}
			else {
				sale_Info = new SalesInfo(price, discount_value, quantity);
				temp_map.put(model_info.getAlphanumericalCode(), sale_Info); // INSERISCO la quantita venduta di quel modello in quella data.
			}
		}
		else { 
			// -> NON avevo ancora venduto dei prodotti in quella data.
			temp_map = new HashMap<>();
			sale_Info = new SalesInfo(price, discount_value, quantity);
			temp_map.put(model_info.getAlphanumericalCode(), sale_Info); // INSERISCO la quantita venduta di quel modello.
			this.dates_map.put(sale_date.format(this.formatter), temp_map); // INSERISCO l'HashMap precedente nella mappa di date.
		}
	}
	
	public void printSalesMap() {
		
		Object keys[] = this.sales_map.keySet().toArray();
		
		System.out.print("\n{\n");
		for(int i = 0; i < keys.length; i++) {
			if(i == keys.length-1) { 
				System.out.print(keys[i] + ", " + this.sales_map.get(keys[i]) + "\n}\n");
			}
			else { 
				System.out.print(keys[i] + ", " + this.sales_map.get(keys[i]) + "\n");
			}
		}
	}
	
	public void printDatesMaps() {
		
		Object keys[] = this.dates_map.keySet().toArray();
		
		for(int i = 0; i < keys.length; i++) {
			HashMap<String, SalesInfo> map_of_date = this.dates_map.get(keys[i]);
			Object temp_keys[] = map_of_date.keySet().toArray();
			System.out.print(keys[i] + ": {");
			for(int j = 0; j < temp_keys.length; j++) {
				System.out.print("-> MODELLO = " + temp_keys[j] + ", " + map_of_date.get(temp_keys[j]).toString() + " | ");		
			}
			System.out.print("}\n");
		}
	}
	
	/**
	 * STATISTICHE VENDITE:
	 * Stampa tutte le vendite effettuate a partire da una certa data specificata.
	 * 
	 * @Param starting_date: Data di inizio da cui stampare le vendite.
	 */
	public void printReportSinceSpecifiedDate(LocalDate starting_date) {
		
		int starting_day = starting_date.getDayOfMonth();
		int starting_month = starting_date.getMonthValue();
		int starting_year = starting_date.getYear();
		
		int current_year = Calendar.getInstance().get(Calendar.YEAR);
		int current_month = Calendar.getInstance().get(Calendar.MONTH) + 1; // TODO: why????
		int current_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		//System.out.println("\nData di Inizio: " + starting_day + "/" + starting_month + "/" + starting_year);
		//System.out.println("Data Attuale: " + current_day + "/" + current_month + "/" + current_year + "\n");

		for(int y = starting_year; y <= current_year; y++) {
			
			int temp_final_month = (y == current_year) ? current_month : 12;
			int temp_start_month = (y == starting_year) ? starting_month: 1;
			
			for(int m = temp_start_month; m <= temp_final_month; m++) {
				
				int temp_final_day = (y == current_year && m == current_month) ? current_day : 31;
				int temp_start_day = (y == starting_year && m == starting_month) ? starting_day : 1;
				
				for(int d = temp_start_day; d <= temp_final_day; d++) {
					LocalDate temp_date;
					try {
						temp_date = LocalDate.of(y, m, d);
					}
					catch (DateTimeException e) {
						//System.out.print("La data " + d + "/" + m + "/" + y + " non è valida!\n");
						continue;
					}
					HashMap<String, SalesInfo> map_of_date = this.dates_map.get(temp_date.format(this.formatter));
					if(map_of_date == null) { continue;} // Non ci sono state vendite in quella data.
					
					Object keys[] = map_of_date.keySet().toArray();
					System.out.print("DATA: " + d + "/" + m + "/" + y + "\n");
					for(int i = 0; i < keys.length; i++) {
						System.out.print("-> MODELLO = " + keys[i] + ", " + map_of_date.get(keys[i]).toString() + " | ");		
					}
					System.out.print("\n");
				}
			}
		}
		
	}
	
	/**
	 * Calcola i soldi guadagnati e lo sconto applicato in totale fino ad oggi: 
	 */
	public void calculateEarnedMoneyandDiscount() {
		
		double total_money = 0.0d;
		double total_discount = 0.0d;
		Object keys[] = this.dates_map.keySet().toArray();
		
		for(int i = 0; i < keys.length; i++) {
			HashMap<String, SalesInfo> map_of_date = this.dates_map.get(keys[i]);
			Object temp_keys[] = map_of_date.keySet().toArray();
			for(int j = 0; j < temp_keys.length; j++) {
				total_money += map_of_date.get(temp_keys[j]).getTotalMoney();
				total_discount += map_of_date.get(temp_keys[j]).getTotalDiscountValue();
			}
		}
		
		System.out.print("\nSoldi guadagnati = " + total_money);
		System.out.print("\nSconto applicato = " + total_discount);
	}
}
