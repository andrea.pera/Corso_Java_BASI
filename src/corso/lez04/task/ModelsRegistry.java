package corso.lez04.task;

import java.util.ArrayList;

/**
 * Registro di tutti i modelli dei prodotti presenti in magazzino.
 */
public class ModelsRegistry {
	
	private ArrayList<ModelInfo> models_list = null; // Lista di modelli distinti presenti in magazzino.
	
	public ModelsRegistry() {
		this.models_list = new ArrayList<ModelInfo>();
	}
	
	/**
	 * Restituisce un ModelInfo con codice univoco per ogni coppia MODELLO-PRODUTTORE.
	 */
	public ModelInfo generateModelInfo(String model, String producer, int quantity) {
		
		ModelInfo model_info;
		
		if(this.models_list.isEmpty()) {
			model_info = new ModelInfo(model, producer);
			model_info.setId(0);
			model_info.setQuantity(quantity);
			model_info.GenerateAlphanumericalCode();
			this.models_list.add(model_info);
			return model_info;
		}
		
		model_info = findModelId(model, producer);
		if(model_info != null) {
			model_info.setQuantity(model_info.getQuantity() + quantity);
			return model_info;
		}
		else { 
			model_info = new ModelInfo(model, producer);
			model_info.setId(getLastId()+1);
			model_info.setQuantity(quantity);
			model_info.GenerateAlphanumericalCode();
			this.models_list.add(model_info);
			return model_info;
		}
	}
	
	private ModelInfo findModelId(String model, String producer) {
		for(int i = 0; i < this.models_list.size(); i++) {
			if(this.models_list.get(i).getModel().equalsIgnoreCase(model) && this.models_list.get(i).getProducer().equalsIgnoreCase(producer)) {
				return this.models_list.get(i);
			}
		}
		return null;
	}
	
	private long getLastId() {
		return this.models_list.get(this.models_list.size()-1).getId();
	}
	
	public ArrayList<ModelInfo> getModelsList() {
		return this.models_list;
	}
	
	public void printModelsList() {
		System.out.print("[ ");
		for(int i = 0; i < this.models_list.size(); i++) {
			if(i == models_list.size()-1) { System.out.print(this.models_list.get(i).getAlphanumericalCode() + " ]\n");}
			else { System.out.print(this.models_list.get(i).getAlphanumericalCode() + ", ");}
		}
	}
	
	public void printQuantitiesList() {
		System.out.print("[ ");
		for(int i = 0; i < this.models_list.size(); i++) {
			if(i == this.models_list.size()-1) { System.out.print(this.models_list.get(i).getQuantity() + " ]\n");}
			else { System.out.print(this.models_list.get(i).getQuantity() + ", ");}
		}
	}
}
