package corso.lez10.task;

public class Trapeze extends Polygon {
	
	private double largest_base;
	private double smaller_base;

	public Trapeze() {}
	
	public Trapeze(double largest_base, double smaller_base, double height) {
		this.setLargestBase(largest_base);
		this.setSmallerBase(smaller_base);
		this.setHeight(height);
	}

	public double getLargestBase() {
		return largest_base;
	}

	public void setLargestBase(double largest_diagonal) {
		this.largest_base = largest_diagonal;
	}

	public double getSmallerBase() {
		return smaller_base;
	}

	public void setSmallerBase(double smaller_diagonal) {
		this.smaller_base = smaller_diagonal;
	}
	
	@Override
	public double area() {
		return ((this.largest_base + smaller_base) * this.height) / 2;
	}
	
	@Override
	public String toString() {
		return "Diagonale Maggiore = " + this.largest_base + "\n" +
			   "Diagonale Minore = " + this.smaller_base + "\n" +
			   "Altezza = " + this.height + "\n" +
			   "Area = " + this.area() + "\n";
	}
}
