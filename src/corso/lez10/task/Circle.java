package corso.lez10.task;

public class Circle extends GeometricElement {

	private double radius;
	private double diameter;
	
	public Circle() {}
	
	public Circle(double radius) {
		this.setRadius(radius);
		this.setDiameter(radius * 2);
	}	
	public Circle(double radius, double diameter) {
		this.setRadius(radius);
		this.setDiameter(diameter);
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}
	
	public double area() {
		return Math.PI * this.radius * this.radius;
	}
	
	public double circumference() {
		return Math.PI * this.diameter;
	}
	
	@Override
	public String toString() {
		return "Raggio = " + this.radius + "\n" +
			   "Diametro = " + this.diameter + "\n" +
			   "Circonferenza = " + this.circumference() + "\n" +
			   "Area = " + this.area() + "\n";
	}
}
