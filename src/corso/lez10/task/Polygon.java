package corso.lez10.task;

public abstract class Polygon extends GeometricElement {

	public Polygon() {}
	
	public Polygon(double base, double height) {
		super(base, height);
	}
	
	public double area() {
		return this.base * this.height;
	}
	
	@Override
	public String toString() {
		return "Base = " + this.base + "\n" +
			   "Altezza = " + this.height + "\n" +
			   "Area = " + this.area() + "\n";
	}
}
