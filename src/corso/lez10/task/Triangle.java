package corso.lez10.task;

public class Triangle extends GeometricElement {
	
	private double sideA;
	private double sideB;
	private double sideC;
	
	public Triangle() {}
	
	public Triangle(double base, double height) {
		super(base, height);
	}
	
	public Triangle(double base, double height, double sideA, double sideB, double sideC) {
		super(base, height);
		this.setSideA(sideA);
		this.setSideB(sideB);
		this.setSideC(sideC);
	}

	public double getSideA() {
		return sideA;
	}

	public void setSideA(double sideA) {
		this.sideA = sideA;
	}

	public double getSideB() {
		return sideB;
	}

	public void setSideB(double sideB) {
		this.sideB = sideB;
	}

	public double getSideC() {
		return sideC;
	}

	public void setSideC(double sideC) {
		this.sideC = sideC;
	}
	
	public double area() {
		return (this.base * this.height) / 2;
	}

	public double perimeter() {
		return this.sideA + this.sideB + this.sideC;
	}
	
	@Override
	public String toString() {
		return "Lato A = " + this.sideA + "\n" +
			   "Lato B = " + this.sideB + "\n" +
			   "Lato C = " + this.sideC + "\n" +
			   "Base = " + this.base + "\n" + 
			   "Altezza = " + this.height + "\n" +
			   "Perimetro = " + this.perimeter() + "\n" +
			   "Area = " + this.area() + "\n";
	}

}
