package corso.lez10.task;

public interface GeometricShape {
	
	public double area();
}
