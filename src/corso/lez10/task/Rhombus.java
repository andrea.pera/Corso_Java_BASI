package corso.lez10.task;

public class Rhombus extends Polygon {
	
	private double largest_diagonal;
	private double smaller_diagonal;

	public Rhombus() {}
	
	public Rhombus(double largest_diagonal, double smaller_diagonal) {
		this.setLargestDiagonal(largest_diagonal);
		this.setSmallerDiagonal(smaller_diagonal);
	}

	public double getLargestDiagonal() {
		return largest_diagonal;
	}

	public void setLargestDiagonal(double largest_diagonal) {
		this.largest_diagonal = largest_diagonal;
	}

	public double getSmallerDiagonal() {
		return smaller_diagonal;
	}

	public void setSmallerDiagonal(double smaller_diagonal) {
		this.smaller_diagonal = smaller_diagonal;
	}
	
	@Override
	public double area() {
		return (this.largest_diagonal * this.smaller_diagonal) / 2;
	}
	
	public double side() {
		return Math.sqrt(Math.pow(smaller_diagonal / 2, 2) + Math.pow(largest_diagonal / 2, 2));
	}
	
	@Override
	public String toString() {
		return "Diagonale Maggiore = " + this.largest_diagonal + "\n" +
			   "Diagonale Minore = " + this.smaller_diagonal + "\n" +
			   "Lato = " + this.side() + "\n" +
			   "Area = " + this.area() + "\n";
	}
}
