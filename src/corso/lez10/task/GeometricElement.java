package corso.lez10.task;

public abstract class GeometricElement implements GeometricShape {
	
	protected double base;
	protected double height;
	
	public GeometricElement() {}
	
	public GeometricElement(double base, double height) {
		this.setBase(base);
		this.setHeight(height);
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	@Override
	public String toString() {
		return "Base = " + this.base + "\n" +
			   "Altezza = " + this.height + "\n";
	}
}
