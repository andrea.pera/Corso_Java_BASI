package corso.lez10.task;

public class Square extends Polygon {
	
	private double side;
	
	public Square() {}
	
	public Square(double side) {
		super(side, side);
		this.setSide(side);
	}
	
	public Square(double base, double height) {
		super(base, height);
		this.setSide(base);
	}
	
	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	@Override
	public double area() {
		return this.side * this.side;
	}
	
	public double diagonal() {
		return this.side * Math.sqrt(2);
	}

	@Override
	public String toString() {
		return "Lato = " + this.side + "\n" + 
			   "Diagonale = " + this.diagonal() + "\n" +
			   "Area = " + this.area() + "\n";
	}
}
