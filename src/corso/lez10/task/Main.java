package corso.lez10.task;

import java.util.ArrayList;
import java.util.List;
import java.util.PrimitiveIterator.OfDouble;

public class Main {

	public static void main(String[] args) {

		GeometricShape gs1 = new Square(15.0d);
		GeometricShape gs2 = new Rectangle(10.5d, 3.0d);
		GeometricShape gs3 = new Triangle(6.0d, 4.0d, 6.0d, 5.0d, 4.5d);
		GeometricShape gs4 = new Circle(13.0d);
		GeometricShape gs5 = new Rhombus(10.5d, 6.5d);
		GeometricShape gs6 = new Trapeze(15.5d, 9.0d, 5.0d);
		GeometricShape gs7 = new Square(20.0d);
		GeometricShape gs8 = new Circle(4.0d);
		
		List<GeometricShape> elements_list = new ArrayList<GeometricShape>();
		elements_list.add(gs1);
		elements_list.add(gs2);
		elements_list.add(gs3);
		elements_list.add(gs4);
		elements_list.add(gs5);
		elements_list.add(gs6);
		elements_list.add(gs7);
		elements_list.add(gs8);
		
		Main.printElements(elements_list);
		
		Main.identifyTypeOfElements(elements_list);
	}
	
	public static void printElements(List<GeometricShape> elements_list) {
		for(int i = 0; i < elements_list.size(); i++) {
			System.out.println("- - Elemento Geometrico " + (i+1) + " - - : " + elements_list.get(i).getClass().getSimpleName() + "\n" + elements_list.get(i).toString());
		}
	}
	
	public static void identifyTypeOfElements(List<GeometricShape> elements_list) {
		for(GeometricShape geometric_element : elements_list) {
			if(geometric_element instanceof Square) { System.out.println("Sono un Quadrato!");}
			else if(geometric_element instanceof Rectangle) { System.out.println("Sono un Rettangolo!");}
			else if(geometric_element instanceof Triangle) { System.out.println("Sono un Triangolo!");}
			else if(geometric_element instanceof Circle) { System.out.println("Sono un Cerchio!");}
			else if(geometric_element instanceof Rhombus) { System.out.println("Sono un Rombo!");}
			else if(geometric_element instanceof Trapeze) { System.out.println("Sono un Trapezio!");}
		}
	}

}
