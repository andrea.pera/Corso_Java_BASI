package corso.lez01.controllisemplici.esercizio;

public class Temperatura {

	public static void main(String[] args) {

		double degree = 32d;
		double MAX_ACCEPTABLE_DEGREE = 37.5d;
		double UPPER_BOUND = 42.0d;
		double LOWER_BOUND = 34.0d;
		
		if(degree <= LOWER_BOUND || degree >= UPPER_BOUND) { 
			System.out.println("Errore!"); 
			System.exit(1);
		}
		if(degree >= MAX_ACCEPTABLE_DEGREE) {
			System.out.println("Non puoi entrare!");		
		}
		else {
			System.out.println("OK, puoi entrare!");		
		}
	}

}
