package corso.lez01.controllisemplici;

public class ControlliSemplici {

	public static void main(String[] args) {
		
		// Variabili Booleane:
		boolean var1 = true;
		boolean var2 = false;
		
		int age = 25;
		int MAGGIORE_ETA = 18;
		
		if(age < 0 || age > 130) { System.out.println("Gli embrioni e i defunti non sono ammessi!");} // Controllo d'errore
		else {
			if(age >= MAGGIORE_ETA) {
				System.out.println("Complimenti, sei maggiorenne!");
			}
			else {
				System.out.println("Niente Birretta per te, sorry :(");
			}
		}
	}

}
