package corso.lez01.stringhe;

public class StringheOperatori {

	public static void main(String[] args) {

		/* - - - - - - - - - - - - - - CONFRONTO TRA STRINGHE: - - - - - - - - - - - - - - */
		String name1 = "luca";
		String name2 = "LUCA";
		
		if(name1.trim().equalsIgnoreCase(name2)) { // Non è "Case Sensitive".
			System.out.println("Stringhe Uguali!\n");
		} 
		else { System.out.println("Stringhe Diverse!\n");}
		
		// OPPURE:
			
		if(name1.toLowerCase().equals(name2.toLowerCase())) { // Metto in minuscolo tutte e due le stringhe.
			System.out.println("Stringhe Uguali!\n");
		} 
		else { System.out.println("Stringhe Diverse!\n");}
		
		// OPPURE:
		
		if(name1.toUpperCase().equals(name2.toUpperCase())) { // Metto in MAIUSCOLO tutte e due le stringhe.
			System.out.println("Stringhe Uguali!\n");
		} 
		else { System.out.println("Stringhe Diverse!\n");}
		
		// RIMOZIONE DEGLI SPAZI: trim()
		
		String name3 = "giovanni ";
		String name4 = "GIOVANNI";
		
		if(name1.trim().equalsIgnoreCase(name2)) { // Metto in MAIUSCOLO tutte e due le stringhe.
			System.out.println("Stringhe Uguali!\n");
		} 
		else { System.out.println("Stringhe Diverse!\n");}
		
		/* - - - - - - - - - - - - - - - RICERCA CONTENUTO STRINGA: - - - - - - - - - - - - - - */
		
		String c = "a";
		
		if(name1.toLowerCase().contains(c.toLowerCase())) { // Ricerca il carattere nella stringa.
			System.out.println("Contenuto\n");
		}
		else { System.out.println("Non contuto\n");}
		
		String frase = "Sono Giovanni e mi piace la programmazione";
		
		if(frase.indexOf("Giovanni") != -1) { // Ricerca se una stringa contiene un'altra stringa. -1 se non la trova.
			System.out.println("Trovato!\n");
		}
		else { System.out.println("Non Trovato!\n");}
		
		/* - - - - - - - - - - - - - - - Caratteri Speciali: - - - - - - - - - - - - - - */
		
		String string = "Sono Giovanni e \"mi piace\" la programmazione";
		
		System.out.println(string + "\n");
		
		/* - - - - - - - - - - - - - - - Controllo dell'Input: - - - - - - - - - - - - - - */
		
		String name = "Marco     ";
		
		name = name.trim(); // Tolgo gli spazi
				
		if(name.isBlank()) { // Controllo che l'Input non sia vuoto.
			System.out.println("Errore!\n");
		}
		else { System.out.println("Ciao, " + name + "\n");}
	}

}
