package corso.lez01.stringhe;

public class Stringhe {

	public static void main(String[] args) {

		//String s = "Ciao, sono una stringa di testo";
		
		//System.out.println(s);
		
		String name1 = "Giovanni Pace";
		int age1 = 10;
		double degree1 = 36.6d;
		
		String tot1 = name1 + " è " + age1 + " anni vecchio ed ha una temperatura corporea di " + degree1 + " gradi";
		
		System.out.println(tot1 + "\n");
		
		String name2 = "Mario Rossi";
		int age2 = 20;
		double degree2 = 35.8d;
		
		String tot2 = name2 + " è " + age2 + " anni vecchio ed ha una temperatura corporea di " + degree2 + " gradi";
		
		System.out.println(tot2);
	}

}
