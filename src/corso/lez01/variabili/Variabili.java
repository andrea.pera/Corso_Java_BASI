package corso.lez01.variabili;

public class Variabili {

	public static void main(String[] args) {

		int a;	// Dichiarazione della variabile.
		a = 5; 	// Assegnazione valore alla variabile.
		
		System.out.println(a);
		
		int b = 10;
		System.out.println("Sum: a + b = " + (a + b)); // Operazioni nelle parentesi vengono eseguite prima.
		
		// Dimensione dei tipi di dato:
		int val = 2147483647; // Valore massimo per una variabile "int";
		System.out.println(val);	
		System.out.println(val + 1); // Out of Bound: Esce dal valore massimo per il valore primitivo "int".
		
		float valore_con_virgola1 = 54.3f; // "f" serve al sistema per capire di che tipo è il dato.
		System.out.println(valore_con_virgola1);	
		
		double valore_con_virgola2 = 40.6d; // Il tipo di dato "double" ha 64 bit di dimensione (il "float" ne ha 32).
		System.out.println(valore_con_virgola2);	
		System.out.println(valore_con_virgola1 + valore_con_virgola2);
		
		System.out.println(15 + 13.1f + 10.20d); // Somma tra intero, float e double.
		
		int numeratore1 = 10;
		int denominatore1 = 3;
		int rapporto1 = numeratore1 / denominatore1;
		System.out.println(rapporto1); // Prende sola la parte intera del risultato.

		float numeratore2 = 10f;
		float denominatore2 = 3f;
		float rapporto2 = numeratore2 / denominatore2;
		System.out.println(rapporto2); // Tutte le variabili sono float, quindi il risultato è un float.


	}

}
