package corso.lez01.controlliavanzati;

public class ControlliAvanzati {

	public static void main(String[] args) {

		// Sistema di traduzione della sigla della provincia in nome effettivo.
		
		String sigla = "AQ";
		
		switch(sigla) {
			default:
				System.out.println("Non nota");
				break;
			case "AQ":
				System.out.println("L'Aquila");
				break;
			case "RM":
				System.out.println("Roma");
				break;
			case "MI":
				System.out.println("Milano");
				break;
		}
		
		// OPPURE:
		
		sigla = "RM";
		
		if(sigla.equals("AQ")) {
			System.out.println("L'Aquila");
		} else if(sigla.equals("BO")) {
			System.out.println("Bologna");
		} else if(sigla.equals("RM")) {
			System.out.println("Roma");
		} else if(sigla.equals("MI")) {
			System.out.println("Milano");
		}
		else {
			System.out.println("Non nota");
		}
		
	}

}
