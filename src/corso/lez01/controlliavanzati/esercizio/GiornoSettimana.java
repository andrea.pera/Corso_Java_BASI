package corso.lez01.controlliavanzati.esercizio;

public class GiornoSettimana {

	public static void main(String[] args) {

		int day = 5;
		String days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

		switch(day) {
			case 1:
				System.out.print("Giorno ITA: Lunedì");
				break;
			case 2:
				System.out.print("Giorno ITA: Martedì");
				break;
			case 3:
				System.out.print("Giorno ITA: Mercoledì");
				break;
			case 4:
				System.out.print("Giorno ITA: Giovedì");
				break;
			case 5:
				System.out.print("Giorno ITA: Venerdì");
				break;
			case 6:
				System.out.print("Giorno ITA: Sabato");
				break;
			case 7:
				System.out.print("Giorno ITA: Domenica");
				break;
			default:
				System.out.print("Error");
				System.exit(1);
		}
		
		System.out.println("\nGiorno ENG: " + days[day-1]);
	}

}
