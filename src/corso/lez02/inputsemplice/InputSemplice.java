package corso.lez02.inputsemplice;

import java.util.Scanner;

public class InputSemplice {

	public static void main(String[] args) {

		Scanner interceptor = new Scanner(System.in);
		System.out.println("Come ti chiami?");
		String name;
		
		do {
			name = interceptor.nextLine();
			if(!name.isBlank()) { // Controllo che l'Input non sia vuoto.
				if(name.length() > 3) {
					break;
				}
			}
			System.out.println("Nome non valido. Riprova!\n");
		} 
		while(true);
		
		name = name.trim(); // Tolgo gli spazi
		
		System.out.println("Ciao, " + name + "!");
		
		System.out.println("Quanti anni hai?\n");
		String age_string;
		int age;
		
		do {
			age_string = interceptor.nextLine();
			if(!age_string.isBlank()) {
				age = Integer.parseInt(age_string); // MANCA IL CONTROLLO DI ERRORE.
				if(age > 0 && age < 130) {
					break;
				}
			}
			System.out.println("Età non valida. Riprova!\n");
		} 
		while(true);
				
		if(age >= 18) { System.out.println("Maggiorenne!");}
		else { System.out.println("Minorenne!");}
		
		interceptor.close();
	}

}
