package corso.lez02.inputsemplice.exercizio;

import java.util.Scanner;

public class IngressoLocale {

	public static void main(String[] args) {
		
		double MAX_ACCEPTABLE_TEMPERATURE = 37.5d;
		double UPPER_BOUND = 42d;
		double LOWER_BOUND = 34d;

		Scanner in = new Scanner(System.in);
		String name;
		String surname;
		String tell_number;
		
		System.out.println("Inserire la temperatura:\n");
		String degree_string = in.nextLine();
		double degree = Double.parseDouble(degree_string);
		
		if(degree <= LOWER_BOUND || degree >= UPPER_BOUND) {
			System.out.println("Errore, non puoi entrare!\n");
		}
		else {
			if(degree > MAX_ACCEPTABLE_TEMPERATURE) { System.out.println("Non puoi entrare!\n");}
			else { 
				System.out.println("Inserire il nome:\n");
				name = in.nextLine();
				if(name.isBlank() || name.length() < 3) { System.out.println("Nome non valido!\n");}
				else { System.out.println("Il tuo nome è: " + name + "\n");}
				
				System.out.println("Inserire il cognome:\n");
				surname = in.nextLine();
				if(surname.isBlank() || surname.length() < 3) { System.out.println("Cognome non valido!\n");}
				else { System.out.println("Il tuo cognome è: " + surname + "\n");}
				
				System.out.println("Inserire il numero di telefono:\n");
				tell_number = in.nextLine();
				if(tell_number.isBlank() || tell_number.length() < 9) { System.out.println("Numero di telefono non valido!\n");}
				else { System.out.println("Il tuo numero di telefono è: " + tell_number + "\n");}
			}
		}
		
		in.close();
	}

}
