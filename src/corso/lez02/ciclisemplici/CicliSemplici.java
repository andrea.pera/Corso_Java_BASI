package corso.lez02.ciclisemplici;

import java.util.Scanner;

public class CicliSemplici {

	public static void main(String[] args) {
		
		//echo();

		/* LISTA PERSONE: */
		String exit = "quit";
		String people = "";
		
		Scanner in = new Scanner(System.in);
		System.out.println("Inserire il nome di una persona o \"quit\" per uscire: ");
		
		while(true) {
			String name = in.nextLine();
			if(name.toLowerCase().equals(exit)) {
				break;
			}
			people = people + name + "\n";
		}
		
		System.out.println("Lista di persone inserite: \n" + people);
		
		in.close();
	}
	
	public static void echo() {
		
		String exit = "quit";
		String echo;
		Scanner in = new Scanner(System.in);
		
		while(true) {
			System.out.println("Inserire il dato o \"quit\" per uscire: ");
			echo = in.nextLine();
			if(echo.toLowerCase().equals(exit)) {
				System.out.println("Passo e chiudo!\n");
				break;
			}
			System.out.println(echo);
		}
		
		in.close();
	}

}
