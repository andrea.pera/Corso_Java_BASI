package corso.lez02.ciclisemplici.esercizio;

import java.util.Scanner;

public class ElencoInvitati {

	public static void main(String[] args) {

		String exit = "quit";
		String lista_persone = "";
		Scanner in = new Scanner(System.in);
		String name;
		String surname;
		
		while(true) {
			System.out.println("Inserimento dati di una nuova persona o \"quit\" per uscire:\nNome:");
			name = in.nextLine();
			if(name.toLowerCase().equals(exit)) { break;}
			if(!check(name)) { System.out.println("Errore! Persona non inserita!");}
			else {
				System.out.println("Cognome:");
				surname = in.nextLine();
				if(surname.toLowerCase().equals(exit)) { break;}
				if(!check(surname)) { System.out.println("Errore! Persona non inserita!");}
				else { 
					lista_persone = lista_persone + name + " " + surname + "\n";
					System.out.println("Inserimento con successo della persona: " + name + " " + surname + "\n");
					System.out.println("Persone attualmente presenti:\n" + lista_persone + "\n");
				}
			}
		}
		
		System.out.println("\nLista completa delle persone inserite con successo:\n" + lista_persone);
		in.close();
	}
	
	public static boolean check(String data) {
		if(data.isBlank() || data.length() < 3) {
			return false;
		}
		return true;
	}
}
