package corso.lez02.contenitoricomplessi.esercizio;

public class StatisticheLibreria {

	public static void main(String[] args) {

		// Contare tutti i libri di "Favilli":
		
		String[] libro_1 = {"Storia della buonanotte", "Favilli", "12345-12345-12345"};
	    String[] libro_2 = {"Nessuno scrive al Federale", "Vitali", "432434-43423-43243"};
	    String[] libro_3 = {"Buonvino e il caso del bambino scomparso", "Veltroni", "76575-765767-765765"};
	    String[] libro_4 = {"Storia del Buongiorno", "Favilli", "12345-12345-12645"};
	    String[] libro_5 = {"Il volo del corvo a Roma", "Veltroni", "76575-765867-765765"};
	    String[] libro_6 = {"Il volo del gabbiano sul tevere", "Veltroni", "76575-765867-765165"};
	    
	    String library[][] = { libro_1, libro_2, libro_3, libro_4, libro_5, libro_6};
	    String autor = "Veltroni";
	    String isbn = "432434-43423-43243";
	    int autor_count = 0;
	    String book_detailString = "";
	    
	    for(int i = 0; i < library.length; i++){
	    	if(library[i][1].equalsIgnoreCase(autor)) {
	    		autor_count++;
	    	}
	    	if(library[i][2].equalsIgnoreCase(isbn)) {
	    		book_detailString = book_detailString + "Titolo del libro: " + library[i][0] + "\nAutore: " + library[i][1];
	    	}
	    }
	    
		System.out.println("I libri dell'autore '" + autor + "' sono " + autor_count);
		System.out.println("\nDettagli del libro con ISBN = '" + isbn + "':\n" + book_detailString);
	}

}
