package corso.lez02.contenitoricomplessi;

public class ContenitoriComplessi {
	
	public static void main(String[] args) {
		
		String mammals[] = {"Cane", "Gatto", "Lupo"};
		String fishes[] = {"Delfino", "Squalo"};
		String birds[] = {"Corvo", "Cornacchia", "Pappagallo"};
		String animals[][] = { mammals, fishes, birds};
		
		for(int i = 0; i < animals.length; i++) {
			print_list(animals[i]);
		}
		
		String voti_esami[][] = {
				{"Analisi I", "27.5"},
				{"Fisica I", "24.3"},
				{"Informatica", "19.8"},
				{"Fisica II", "25.0"},
		};
		
		String voti_esami_pesati[][] = {
				{"Analisi I", "6", "27.5"},
				{"Fisica I", "9", "24.3"},
				{"Informatica", "6", "19.8"},
				{"Fisica II", "9", "25.0"},
		};
		
		System.out.println("\nMedia Aritmetica: " + aritmeticMean(voti_esami));
		
		System.out.println("Media Ponderata: " + ponderateMean(voti_esami_pesati));
	}
	
	public static void print_list(String list[]) {
		
		for(int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println("");
	}
	
	public static double aritmeticMean(String voti_esami[][]) {
		
		double mean = 0.0d;
		double sum = 0.0d;
		
		for(int i = 0; i < voti_esami.length; i++) {
			sum = sum + Double.parseDouble(voti_esami[i][1]);
		}
		
		mean = sum / voti_esami.length;
		
		return mean;
	}
	
	public static double ponderateMean(String voti_esami_ponderati[][]) {
		
		double mean = 0.0d;
		double sum = 0.0d;
		double weights_sum = 0.0d;
		
		for(int i = 0; i < voti_esami_ponderati.length; i++) {
			sum = sum + Double.parseDouble(voti_esami_ponderati[i][1]) * Double.parseDouble(voti_esami_ponderati[i][2]);
			weights_sum = weights_sum + Double.parseDouble(voti_esami_ponderati[i][1]);
		}
		
		mean = sum / weights_sum;
		
		return mean;
	}
}
