package corso.lez02.contenitorisemplici;

import java.util.Scanner;

public class ContenitoriArray {

	public static void main(String[] args) {

		String cars[] = new String[6];
				
		cars[0] = "Ferrari";
		cars[1] = "Maserati";
		cars[2] = "FIAT";
		cars[3] = "BMW";
		cars[4] = "Porches";
		cars[5] = "Lamborghini";
		
		System.out.println("Elenco Automobili:\n");
		for(int i = 0; i < cars.length; i++) {
			System.out.println(cars[i]);
		}
		
		String car_to_find = "FIAT";
		boolean found = false;
		int index = -1;
		
		for(int i = 0; i < cars.length; i++) {
			if(cars[i].equals(car_to_find)) {
				found = true;
				index = i;
				break;
			}	
		}
		
		if(found) {
			System.out.println("\nElemento '" + car_to_find + "' trovato in posizione " + index);
		}
		else {
			System.out.println("\nElemento '" + car_to_find + "' non trovato");
		}
		
		/* - - - - - - - - - - CONTEGGIO ELEMENTI CONTENUTI - - - - - - - - - */

		String exit = "quit";
		Scanner in = new Scanner(System.in);
		
		String animals[] = { "Cane", "Gatto", "Topo", "Babuino", "Carpa", "Gatto", "Lontra", "Bradipo", "Pinguino", "Gatto", "Lontra", "Gatto"};
		
		do {
			System.out.println("\nInserire l'animale da ricercare o 'quit' per uscire:");
			String input = in.nextLine();
			if(input.toLowerCase().equals(exit)) { break;}
			if(input.isBlank()) {
				System.out.println("Inserimento non valido!");
				continue;
			}
			int count = trovaOccorrenze(animals, input);
			System.out.println("L'animale inserito è presente " + count + " volte nella lista!");
			
		} while(true);
		
		System.out.println("Fine.");
		
		in.close();
	}
	
	public static int trovaOccorrenze(String[] lista, String s) {
		
		int count = 0;
		
		for(int i = 0; i < lista.length; i++) {
			if(lista[i].equalsIgnoreCase(s)) {
				count++;
			}	
		}
		
		return count;
	}
}
