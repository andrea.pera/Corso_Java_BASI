package corso.lez11.esercizio;

public class Restaurant implements BusinessActivity {
	
	private Menu restaurant_menu = null;
	
	public Restaurant() {
		this.restaurant_menu = new Menu();
	}
	
	public void insertBusinessArticle(BusinessArticle article) {
		this.restaurant_menu.addArticle(article);
	}
	
	public boolean removeSpecifiedBusinessArticle(BusinessArticle article) {
		return this.restaurant_menu.removeArticle(article);
	}
	
	public boolean removeSpecifiedBusinessArticleByCode(int code) {
		return this.restaurant_menu.removeArticleByCode(code);
	}
	
	public void printAllBusinessArticles() {
		this.restaurant_menu.printMenu();
	}
}
