package corso.lez11.esercizio;

public interface BusinessActivity {

	public void insertBusinessArticle(BusinessArticle article);
	
	public boolean removeSpecifiedBusinessArticle(BusinessArticle article);
	
	public boolean removeSpecifiedBusinessArticleByCode(int code);
	
	public void printAllBusinessArticles();
}
