package corso.lez11.esercizio;

public abstract class BusinessArticle {
	
	protected static int ID = 0;
	protected int code;
	protected String type;
	protected String name;
	protected double price;
	
	public BusinessArticle() {
		ID++;
		this.setCode(ID);
	}
	
	public BusinessArticle(String name, double price) {
		ID++;
		this.setCode(ID);
		this.setName(name);
		this.setPrice(price);
	}
	
	public BusinessArticle(String type, String name, double price) {
		ID++;
		this.setCode(ID);
		this.setType(type);
		this.setName(name);
		this.setPrice(price);
	}

	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Codice: " + code + "\n" +
			   "Tipo: " + this.type + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Prezzo: " + this.price + "\n";
	}

}
