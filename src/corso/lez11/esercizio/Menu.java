package corso.lez11.esercizio;

import java.util.ArrayList;
import java.util.List;

public class Menu {
	
	private List<BusinessArticle> articles = null;
	
	public Menu() {
		this.articles = new ArrayList<BusinessArticle>();
	}

	public List<BusinessArticle> getBusinessArticles() {
		return articles;
	}

	public void setBusinessArticles(List<BusinessArticle> business_articles) {
		this.articles = business_articles;
	}
	
	public void addArticle(BusinessArticle article) {
		this.articles.add(article);
	}
	
	public boolean removeArticle(BusinessArticle article) {
		try {
			this.articles.remove(article);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean removeArticleByCode(int code) {
		int index = -1;
		for(int i = 0; i < this.articles.size(); i++) {
			if(this.articles.get(i).getCode() == code) {
				index = i;
				break;
			}
		}
		if(index != -1) {
			this.articles.remove(index);
			return true;
		}
		else {
			return false;
		}
	}
	
	public void printMenu() {
		System.out.println("- - - - Menu Completo - - - - :\n");
		for(BusinessArticle article: this.articles) {
			System.out.println(article.toString() + "\n");
		}
	}
}
