package corso.lez11.esercizio;

public class Drink extends BusinessArticle {
	
	public Drink() {
		super();
	}
	
	public Drink(String name, double price) {
		super(name, price);
		this.setType("Drink");
	}
	
	public Drink(String type, String name, double price) {
		super(type, name, price);
	}

}
