package corso.lez11.esercizio;

public class Dish extends BusinessArticle {
	
	private boolean isVegan;

	public Dish() {
		super();
	}
	
	public Dish(String name, double price, boolean isVegan) {
		super(name, price);
		this.setType("Dish");
		this.setIsVegan(isVegan);
	}
	
	public Dish(String type, String name, double price, boolean isVegan) {
		super(type, name, price);
		this.setIsVegan(isVegan);
	}

	public boolean GetIsVegan() {
		return isVegan;
	}

	public void setIsVegan(boolean isVegan) {
		this.isVegan = isVegan;
	}
	
	@Override
	public String toString() {
		return "Codice: " + code + "\n" +
			   "Tipo: " + this.type + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Vegano: " + this.isVegan + "\n" +
			   "Prezzo: " + this.price + "\n";
	}
}
