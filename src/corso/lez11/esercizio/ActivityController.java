package corso.lez11.esercizio;

public class ActivityController {
	
    private static ActivityController instance = null;
    private static BusinessActivity myActivity = null;

    private ActivityController(String type) {
    	switch(type) {
			case "Restaurant":
				myActivity = new Restaurant();				
				break;	
			default:
				System.out.println("Tipo di attivita non riconosciuto!");
				break;
		}
    } 
 
    public static ActivityController getInstance(String type) {
        if (instance == null) {
            instance = new ActivityController(type);
        }
        return instance;
    }
    
    /**
     * Inserisce un nuovo articolo.
     * @param type: Drink | Dish
     */
    public BusinessArticle insertBusinessArticle(String type, String name, double price, boolean isVegan) {
    	BusinessArticle new_Article = null;
    	switch(type) {
			case "Drink":
				new_Article = new Drink(name, price);
				break;	
			case "Dish":
				new_Article = new Dish(name, price, isVegan);
				break;
			default:
				System.out.println("Tipo di articolo non riconosciuto!\n");
				return null;
		}
    	myActivity.insertBusinessArticle(new_Article);
    	System.out.println("Nuovo articolo inserito con successso!\n");
    	return new_Article;
    }
    
	public void removeSpecifiedBusinessArticle(BusinessArticle article) {
		if(myActivity.removeSpecifiedBusinessArticle(article)) { System.out.println("Articolo rimosso con successso!\n");}
		else { System.out.println("L'articolo non può essere rimosso!\n");}
	}
	
	public void removeSpecifiedBusinessArticleByCode(int code) {
		if(myActivity.removeSpecifiedBusinessArticleByCode(code)) { System.out.println("Articolo rimosso con successso!\n");}
		else { System.out.println("L'articolo non può essere rimosso!\n");}
	}
	
	public void printAllBusinessArticles() {
		myActivity.printAllBusinessArticles();
	}
}
