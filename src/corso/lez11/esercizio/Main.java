package corso.lez11.esercizio;

public class Main {
	
	/*
	* Creare un sistema di gestione menu per uno o più ristoranti
	* Ogni ristorante avrà due categorie di articoli: bevande, piatti
	*
	* Creare dei metodi in grado di:
	* - Inserire una bevanda o un piatto
	* - Stampare tutte le bevande o piatti (a scelta)
	* - Contare tutte le bevande o piatti o entrambi
	*
	* Ogni articolo avrà ALMENO:
	* - Codice
	* - Nome
	* - Prezzo
	*/
	
	public static final String DRINK_TYPE = "Drink";
	public static final String DISH_TYPE = "Dish";
	public static final String RESTAURANT_TYPE = "Restaurant";

	public static void main(String[] args) {

		ActivityController activity_controller = ActivityController.getInstance(RESTAURANT_TYPE);
		
		activity_controller.insertBusinessArticle(DRINK_TYPE, "Rum e Cola", 5.0d, false);
		activity_controller.insertBusinessArticle(DRINK_TYPE, "Vodka", 3.0d, false);
		activity_controller.insertBusinessArticle(DRINK_TYPE, "Spritz", 2.0d, false);
		
		activity_controller.insertBusinessArticle(DISH_TYPE, "Pasta al Sugo", 10.0d, true);
		activity_controller.insertBusinessArticle(DISH_TYPE, "Bistecca", 25.0d, false);
		activity_controller.insertBusinessArticle(DISH_TYPE, "Riso al Curry", 15.0d, true);
		activity_controller.insertBusinessArticle(DISH_TYPE, "Fettina Panata", 2.0d, false);
		
		activity_controller.printAllBusinessArticles();
		
		activity_controller.removeSpecifiedBusinessArticleByCode(1);
		activity_controller.removeSpecifiedBusinessArticleByCode(5);
		
		activity_controller.printAllBusinessArticles();
	}

}
