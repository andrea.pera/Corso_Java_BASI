package corso.lez06.magazzinopersone;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * CODICE FISCALE di una persona.
 */
public class IdentityCode { 

	private String code;
	private LocalDate expiration_date;
	
	public IdentityCode() {}
	
	public IdentityCode(String code, LocalDate expiration_date) {
		this.setCode(code);
		this.setExpirationDate(expiration_date);
	}
	
	public String getCode() {
		return this.code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public LocalDate getExpiratioDate() {
		return this.expiration_date;
	}
	
	public void setExpirationDate(LocalDate expiration_date) {
		this.expiration_date = expiration_date;
	}
	
	public void printIdentityCode() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
		
		System.out.println(
				"Codice Fiscale: " + this.code + 
				"\nData di Scadenza: " + this.expiration_date.format(formatter)
				);
	}
}
