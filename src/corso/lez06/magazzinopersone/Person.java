package corso.lez06.magazzinopersone;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Person {
	
	private String name;
	private String surname;
	private LocalDate birth_date;
	private IdentityCard identity_card;
	private IdentityCode identity_code;
	
	public Person() {}
	
	public Person(String name, String surname, LocalDate birth_date) {
		this.setName(name);
		this.setSurname(surname);
		this.setBirthDate(birth_date);
	}
	
	public Person(String name, String surname, LocalDate birth_date, IdentityCard identity_card, IdentityCode identity_code) {
		this.setName(name);
		this.setSurname(surname);
		this.setBirthDate(birth_date);
		this.setIdentityCard(identity_card);
		this.setIdentityCode(identity_code);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getBirthDate() {
		return birth_date;
	}

	public void setBirthDate(LocalDate birth_date) {
		this.birth_date = birth_date;
	}

	public IdentityCard getIdentityCard() {
		return identity_card;
	}

	public void setIdentityCard(IdentityCard identity_card) {
		this.identity_card = identity_card;
	}

	public IdentityCode getIdentityCode() {
		return identity_code;
	}

	public void setIdentityCode(IdentityCode identity_code) {
		this.identity_code = identity_code;
	}
	
	public void printPerson() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
		
		System.out.println(
				"\nNome: " + this.name + 
				"\nCognome: " + this.surname +
				"\nData di Nascita: " + this.birth_date.format(formatter));
		
		this.identity_code.printIdentityCode();
		this.identity_card.printIdentityCard();
		System.out.println("\n");
	}
	
	
}
