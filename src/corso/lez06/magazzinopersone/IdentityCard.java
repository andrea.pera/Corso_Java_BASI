package corso.lez06.magazzinopersone;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * CARTA DI IDENTITA di una persona.
 */
public class IdentityCard {

	private String code;
	private LocalDate release_date;
	private LocalDate expiration_date;
	
	public IdentityCard() {}
	
	public IdentityCard(String code, LocalDate release_date, LocalDate expiration_date) {
		this.setCode(code);
		this.setRelease_date(release_date);
		this.setExpiration_date(expiration_date);
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public LocalDate getRelease_date() {
		return release_date;
	}
	
	public void setRelease_date(LocalDate release_date) {
		this.release_date = release_date;
	}
	
	public LocalDate getExpiration_date() {
		return expiration_date;
	}
	
	public void setExpiration_date(LocalDate expiration_date) {
		this.expiration_date = expiration_date;
	}
	
	public void printIdentityCard() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy").withLocale(Locale.ITALY);
		
		System.out.println(
				"Codice Carta di Identita: " + this.code + 
				"\nData di Rilascio: " + this.release_date.format(formatter) +
				"\nData di Scadenza: " + this.expiration_date.format(formatter)
				);
	}
}
