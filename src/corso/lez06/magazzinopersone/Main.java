package corso.lez06.magazzinopersone;

import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		
		Person person1 = new Person("Mario", "Rossi", LocalDate.of(1990, 1, 10));
		person1.setIdentityCode(new IdentityCode("CODICE_FISCALE_PERSONA_1", LocalDate.of(2030, 1, 15)));
		person1.setIdentityCard(new IdentityCard("CODICE_CARTA_PERSONA_1", LocalDate.of(2020, 9, 21), LocalDate.of(2031, 9, 21)));
		
		Person person2 = new Person("Giovanni", "Verdi", LocalDate.of(1970, 5, 6));
		person2.setIdentityCode(new IdentityCode("CODICE_FISCALE_PERSONA_2", LocalDate.of(2028, 2, 1)));
		person2.setIdentityCard(new IdentityCard("CODICE_CARTA_PERSONA_2", LocalDate.of(2010, 10, 2), LocalDate.of(2024, 10, 2)));
		
		person1.printPerson();

		person2.printPerson();
		
	}
}
