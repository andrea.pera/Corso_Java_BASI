package corso.lez18.justdeliveroo.controller;

import java.util.List;
import corso.lez18.justdeliveroo.data.CommandDAO;
import corso.lez18.justdeliveroo.data.CustomerDAO;
import corso.lez18.justdeliveroo.data.MenuDAO;
import corso.lez18.justdeliveroo.data.RestaurantDAO;
import corso.lez18.justdeliveroo.data.RiderDAO;
import corso.lez18.justdeliveroo.model.Command;
import corso.lez18.justdeliveroo.model.Customer;
import corso.lez18.justdeliveroo.model.Menu;
import corso.lez18.justdeliveroo.model.Restaurant;
import corso.lez18.justdeliveroo.model.Rider;

public class DatabaseController {

    private static DatabaseController instance = null;

    private DatabaseController() {} 
 
    public static DatabaseController getInstance() {
        if (instance == null) {
            instance = new DatabaseController();
        }
        return instance;
    }
    
    public Customer addCustomer(String name, String surname, String address) {
    	return CustomerDAO.insertNewCustomer(name, surname, address);
    }
    
    public Rider addRider(String name, String surname, String position, String vehicle_type, int available_space) {
    	//TODO: Controllo dei parametri.
    	return RiderDAO.insertNewRider(name, surname, position, vehicle_type, available_space);
    }
    
    public boolean removeRider(int rider_id) {
    	return RiderDAO.deleteRiderById(rider_id);
    }
    
    public boolean changeRiderPosition(int rider_id, String new_position) {
    	return RiderDAO.changePositionById(rider_id, new_position);
    }
    
    public boolean changeRiderVehicleType(int rider_id, String new_vehicle_type) {
    	return RiderDAO.changeVehicleTypeById(rider_id, new_vehicle_type);
    }
    
    public Rider findRiderByName(String name, String surname) {
    	if(name.equals("") || surname.equals("")) {
    		return null;
    	}
    	else {
    		return RiderDAO.findRiderByName(name, surname);
    	}
    }
    
    public List<Restaurant> findRestaurantWithMinAveragePrice() {
    	return RestaurantDAO.findRestaurantWithMinAveragePrice();
    }
    
    public List<Rider> getAllRiders() {
    	return RiderDAO.getAllRiders();
    }
    
    public List<Restaurant> getAllRestaurants() {
    	return RestaurantDAO.getAllRestaurants();
    }
    
    public List<Customer> getAllCustomers() {
    	return CustomerDAO.getAllCustomers();
    }
    
    public List<Command> getAllCommands() {
    	return CommandDAO.getAllCommands();
    }
    
    public List<Menu> getAllMenus() {
    	return MenuDAO.getAllMenus();
    }
}
