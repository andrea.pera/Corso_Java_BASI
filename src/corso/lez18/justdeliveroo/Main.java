package corso.lez18.justdeliveroo;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;
import corso.lez18.justdeliveroo.controller.DatabaseController;
import corso.lez18.justdeliveroo.data.JDBCMySQLConnection;
import corso.lez18.justdeliveroo.model.Customer;
import corso.lez18.justdeliveroo.model.Rider;

public class Main {

	public static void main(String[] args) {

		//testDBConnection();
		
		DatabaseController controller = DatabaseController.getInstance();
		
		//List<Object> restaurants_list = new ArrayList<Object>(controller.getAllRestaurants());
		//Main.printDataList(restaurants_list);
		
		//List<Object> commands_list = new ArrayList<Object>(controller.getAllCommands());
		//Main.printDataList(commands_list);
		
		//List<Object> menus_list = new ArrayList<Object>(controller.getAllMenus());
		//Main.printDataList(menus_list);
		
		//List<Object> restaurants_list = new ArrayList<Object>(controller.findRestaurantWithMinAveragePrice());
		//Main.printDataList(restaurants_list, "- - Ristorante/i con il MINORE Prezzo Medio - - :\n");
		
		// - - - - - - RIDER - - - - - - - - - - :
		
		//Rider rider = controller.findRiderByName("Federico", "Verdi");
		//Main.printData(rider);
		
		//Rider rider = controller.addRider("Mario", "Rossi", "Via Anagnina 34, Roma", "Auto", 1);
		//Main.printData(rider, "Inserimento");
		
		//boolean result = controller.removeRider(16);
		//Main.printData(result, "Rimozione");
		
		//boolean result = controller.changeRiderPosition(15, "Nuova Posizione");
		//Main.printData(result, "Cambio Posizione");
		
		//boolean result = controller.changeRiderVehicleType(15, "Moto");
		//Main.printData(result, "Cambio Tipo Veicolo");
		
		//List<Object> riders_list = new ArrayList<Object>(controller.getAllRiders());
		//Main.printDataList(riders_list);
		
		// - - - - - - CUSTOMER - - - - - - - - - - :
		
		//Customer new_customer = controller.addCustomer("Emilio", "Bianchi", "Via del Politecnico 1, Torvegata (RM)");
		//Main.printData(new_customer, "Inserimento");
		
		List<Object> customers_list = new ArrayList<Object>(controller.getAllCustomers());
		Main.printDataList(customers_list);
	}
	
	public static void testDBConnection() {
		Connection connection = null;
		try {
			connection = JDBCMySQLConnection.getConnection();
			connection.createStatement();			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		System.out.println("Connected Successfully!\n");
	}
	
	public static void printData(Object obj) {
		if(obj == null) { System.out.println("Nessun Elemento Trovato.");}
		else { System.out.println(obj.toString());}
	}
	
	public static void printData(boolean result, String message) {
		if(result) { System.out.println(message + " Avvenuto con Successo.");}
		else { System.out.println(message + " Non Riuscito.");}
	}

	public static void printData(Object obj, String message) {
		if(obj == null) { System.out.println(" Non Riuscito.");}
		else { 
			System.out.println(message + " Avvenuto con Successo.\n");
			System.out.println(obj.toString());
		}
	}
	
	public static void printDataList(List<Object> obj_list) {
		System.out.println("- - - - - LISTA di " + obj_list.get(0).getClass().getSimpleName() + " - - - - - :\n");
		for(Object obj: obj_list) {
			System.out.println(obj.toString());
		}
	}
	
	public static void printDataList(List<Object> obj_list, String message) {
		System.out.println(message);
		for(Object obj: obj_list) {
			System.out.println(obj.toString());
		}
	}
}
