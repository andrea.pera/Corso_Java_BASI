package corso.lez18.justdeliveroo.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez18.justdeliveroo.model.Command;

public class CommandDAO {
	
	public static List<Command> getAllCommands() {
		
		List<Command> commands_list = new ArrayList<Command>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Command.class.getSimpleName();
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Command command = new Command(result.getString("name"),
            					  result.getString("delivery_address"),
            					  result.getDouble("price"),
            					  result.getInt("rider_id"),
            					  result.getInt("restaurant_id"),
            					  result.getInt("customer_id"));
            	command.setId(result.getInt("id"));
            	commands_list.add(command);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return commands_list;
	}

}
