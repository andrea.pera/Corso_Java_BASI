package corso.lez18.justdeliveroo.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez18.justdeliveroo.model.Menu;

public class MenuDAO {
	
	public static List<Menu> getAllMenus() {
		
		List<Menu> menus_list = new ArrayList<Menu>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Menu.class.getSimpleName();
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Menu menu = new Menu(result.getString("name"),
            					  result.getString("description"),
            					  result.getDouble("price"),
            					  result.getInt("restaurant_id"));
            	menu.setId(result.getInt("id"));
            	menus_list.add(menu);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return menus_list;
	}

}
