package corso.lez18.justdeliveroo.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez18.justdeliveroo.model.Customer;

public class CustomerDAO {
	
	public static Customer insertNewCustomer(String name, String surname, String address) {
		
		Customer rider = null;		
		ResultSet resultSet = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null; 
        String query = "INSERT INTO " + Customer.class.getSimpleName() + " (name, surname, address) " +
                	   "VALUES (?, ?, ?)";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, address);
            
            preparedStatement.execute();
            
            resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()) {         
	        	rider = new Customer(name, surname, address); 
	        	rider.setId(resultSet.getInt(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return rider;	
	}
	
	public static List<Customer> getAllCustomers() {
		
		List<Customer> customers_list = new ArrayList<Customer>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Customer.class.getSimpleName();
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Customer customer = new Customer(result.getString("name"),
            					  result.getString("surname"),
            					  result.getString("address"));
            	customer.setId(result.getInt("id"));
            	customers_list.add(customer);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return customers_list;
	}

}
