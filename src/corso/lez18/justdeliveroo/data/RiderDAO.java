package corso.lez18.justdeliveroo.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez18.justdeliveroo.model.Rider;

public class RiderDAO {
	
	public static Rider insertNewRider(String name, String surname, String position, String vehicle_type, int available_space) {
		
		Rider rider = null;		
		ResultSet resultSet = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null; 
        String query = "INSERT INTO " + Rider.class.getSimpleName() + " (name, surname, position, vehicle_type, available_space) " +
                	   "VALUES (?, ?, ?, ?, ?)"; // ? = Parametri
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, name); // Parametro 1.
            preparedStatement.setString(2, surname);
            preparedStatement.setString(3, position);
            preparedStatement.setString(4, vehicle_type);
            preparedStatement.setInt(5, available_space);
            
            preparedStatement.execute();
            
            resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()) {         
	        	rider = new Rider(name, surname, position, vehicle_type, available_space); 
	        	rider.setId(resultSet.getInt(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return rider;	
	}
	
	public static boolean deleteRiderById(int rider_id) {
		
		boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null; 
        String query = "DELETE FROM " + Rider.class.getSimpleName() + " " + 
        			   "WHERE id = ?";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, rider_id);
            
            preparedStatement.execute();
            result = true;
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;
	}
	
	public static boolean changePositionById(int rider_id, String new_position) {
		
		boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null; 
        String query = "UPDATE " + Rider.class.getSimpleName() + " " + 
        			   "SET position = ? " +
        			   "WHERE id = ?";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, new_position);
            preparedStatement.setInt(2, rider_id); 
            		
            preparedStatement.execute();
            result = true;
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;
	}
	
	public static boolean changeVehicleTypeById(int rider_id, String new_vehicle_type) {
		
		boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null; 
        String query = "UPDATE " + Rider.class.getSimpleName() + " " + 
        			   "SET vehicle_type = ? " +
        			   "WHERE id = ?";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, new_vehicle_type);
            preparedStatement.setInt(2, rider_id); 
            		
            preparedStatement.execute();
            result = true;
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;
	}
	
	public static Rider findRiderByName(String name, String surname) {
		
		Rider rider = null;		
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Rider.class.getSimpleName() +
        			   "WHERE name = '" + name + "' && " + "surname = '" + surname + "'";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            if(result.next()) {
            	rider = new Rider(result.getString("name"),
            					  result.getString("surname"),
            					  result.getString("position"),
            					  result.getString("vehicle_type"),
            					  result.getInt("available_space"));
            	rider.setId(result.getInt("id")); // "id" = nome della COLONNA del DB.
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return rider;
	}
	
	public static List<Rider> getAllRiders() {
		
		List<Rider> riders_list = new ArrayList<Rider>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM Rider";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Rider rider = new Rider(result.getString("name"),
            					  result.getString("surname"),
            					  result.getString("position"),
            					  result.getString("vehicle_type"),
            					  result.getInt("available_space"));
            	rider.setId(result.getInt("id"));
            	riders_list.add(rider);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return riders_list;
	}

}
