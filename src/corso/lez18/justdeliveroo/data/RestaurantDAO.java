package corso.lez18.justdeliveroo.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez18.justdeliveroo.model.Restaurant;

public class RestaurantDAO {
	
	public static List<Restaurant> findRestaurantWithMinAveragePrice() {
		
		List<Restaurant> restaurants_list = new ArrayList<Restaurant>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Restaurant.class.getSimpleName() + " " +
        			   "WHERE average_price = (SELECT MIN(average_price) " +
        			   						  "FROM " + Restaurant.class.getSimpleName() + ")";
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Restaurant restaurant = new Restaurant(result.getString("name"),
            					  result.getString("address"),
            					  result.getString("style"),
            					  result.getDouble("average_price"));
            	restaurant.setId(result.getInt("id"));
            	restaurants_list.add(restaurant);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return restaurants_list;
	}
	
	public static List<Restaurant> getAllRestaurants() {
		
		List<Restaurant> restaurants_list = new ArrayList<Restaurant>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Restaurant.class.getSimpleName();
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Restaurant restaurant = new Restaurant(result.getString("name"),
            					  result.getString("address"),
            					  result.getString("style"),
            					  result.getDouble("average_price"));
            	restaurant.setId(result.getInt("id"));
            	restaurants_list.add(restaurant);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return restaurants_list;
	}

}
