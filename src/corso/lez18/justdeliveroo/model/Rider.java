package corso.lez18.justdeliveroo.model;

import java.util.ArrayList;
import java.util.List;

public class Rider {

	private int id;
	private String name;
	private String surname;
	private String position;
	private String vehicle_type;
	private int available_space;
	private List<Command> orders = new ArrayList<Command>();
	
	public Rider() {}
	
	public Rider(String name, String surname, String position, String vehicle, int available_space) {
		this.setName(name);
		this.setSurname(surname);
		this.setPosition(position);
		this.setVehicleType(vehicle);
		this.setAvailableSpace(available_space);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getAvailableSpace() {
		return available_space;
	}

	public void setAvailableSpace(int available_space) {
		this.available_space = available_space;
	}

	public String getVehicleType() {
		return vehicle_type;
	}

	public void setVehicleType(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	public List<Command> getOrders() {
		return orders;
	}

	public void addOrder(Command new_order) {
		this.orders.add(new_order);
	}
	
	public boolean removeOrder(Command order) {
		return this.orders.remove(order);
	}

	@Override
	public String toString() {
		return "ID: " + this.id + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Cognome: " + this.surname + "\n" +
			   "Posizione: " + this.position + "\n" +
			   "Veicolo: " + this.vehicle_type + "\n" +
			   "Spazio Disponibile: " + this.available_space + "\n";
	}
}
