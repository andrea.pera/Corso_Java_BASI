package corso.lez18.justdeliveroo.model;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
	
	private int id;
	private String name;
	private String address;
	private String style;
	private double average_price;
	private List<Menu> menus = new ArrayList<Menu>();
	
	public Restaurant() {}
	
	public Restaurant(String name, String address, String style, double average_price) {
		this.setName(name);
		this.setAddress(address);
		this.setStyle(style);
		this.setAveragePrice(average_price);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public double getAveragePrice() {
		return average_price;
	}

	public void setAveragePrice(double average_price) {
		this.average_price = average_price;
	}
	
	public void addMenu(Menu new_menus) {
		this.menus.add(new_menus);
	}
	
	public boolean removeMenu(Menu menu) {
		return this.menus.remove(menu);
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Indirizzo: " + this.address + "\n" +
			   "Stile: " + this.style + "\n" +
			   "Prezzo Medio: " + this.average_price + "\n";
	}
}
