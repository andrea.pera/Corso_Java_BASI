package corso.lez18.justdeliveroo.model;

public class Menu {
	
	private int id;
	private String name;
	private String description;
	private double price;
	private int restaurant_id;
	
	public Menu() {}
	
	public Menu(String name, double price) {
		this.setName(name);
		this.setPrice(price);
	}
	
	public Menu(String name, String description, double price, int restaurant_id) {
		this.setName(name);
		this.setDescription(description);
		this.setPrice(price);
		this.setRestaurantId(restaurant_id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getRestaurantId() {
		return restaurant_id;
	}

	public void setRestaurantId(int restaurant_id) {
		this.restaurant_id = restaurant_id;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Descrizione: " + this.description + "\n" +
			   "Prezzo: " + this.price + "\n" +
			   "ID Ristorante: " + this.restaurant_id + "\n";
	}
}
