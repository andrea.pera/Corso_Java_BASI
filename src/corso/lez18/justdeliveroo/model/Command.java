package corso.lez18.justdeliveroo.model;

public class Command { // Ordine
	
	private int id;
	private String name;
	private String delivery_address;
	private double price;
	private int restaurant_id;
	private int rider_id;
	private int customer_id;
	
	public Command() {}
	
	public Command(String name, String delivery_address, double price, int restaurant_id, int rider_id, int customer_id) {
		this.setName(name);
		this.setDeliveryAddress(delivery_address);
		this.setPrice(price);
		this.setRestaurantId(restaurant_id);
		this.setRiderId(rider_id);
		this.setCustomerId(customer_id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDeliveryAddress() {
		return delivery_address;
	}

	public void setDeliveryAddress(String delivery_address) {
		this.delivery_address = delivery_address;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getRestaurantId() {
		return restaurant_id;
	}

	public void setRestaurantId(int restaurant_id) {
		this.restaurant_id = restaurant_id;
	}

	public int getRiderId() {
		return rider_id;
	}

	public void setRiderId(int rider_id) {
		this.rider_id = rider_id;
	}
	
	public int getCustomerId() {
		return customer_id;
	}

	public void setCustomerId(int customer_id) {
		this.customer_id = customer_id;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Indirizzo di Consegna: " + this.delivery_address + "\n" +
			   "Prezzo: " + this.price + "\n" +
			   "ID Ristorante: " + this.restaurant_id + "\n" +
			   "ID Rider: " + this.rider_id + "\n" +
			   "ID Customer: " + this.customer_id + "\n";
	}
}
