package corso.lez18.justdeliveroo.model;

public class Customer {
	
	private int id;
	private String name;
	private String surname;
	private String address;
	
	public Customer() {}
	
	public Customer(String name, String surname, String address) {
		this.setName(name);
		this.setSurname(surname);
		this.setAddress(address);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + "\n" +
			   "Nome: " + this.name + "\n" +
			   "Cognome: " + this.surname + "\n" +
			   "Indirizzo: " + this.address + "\n";
	}
}
