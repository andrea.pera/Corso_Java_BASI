package corso.lez20.spotifoo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import corso.lez20.spotifoo.data.CustomerDAO;
import corso.lez20.spotifoo.data.JDBCMySQLConnection;

public class Main {
	
	public static void main(String[] args) {

		//testDBConnection();
		
		Main.printDataList(new ArrayList<Object>(CustomerDAO.getAllCustomers()));
	}
	
	
	public static void testDBConnection() {
		Connection connection = null;
		try {
			connection = JDBCMySQLConnection.getConnection();
			connection.createStatement();			
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		System.out.println("Connected Successfully!\n");
	}
	
	public static void printDataList(List<Object> obj_list) {
		System.out.println("- - - - - LISTA di " + obj_list.get(0).getClass().getSimpleName() + " - - - - - :\n");
		for(Object obj: obj_list) {
			System.out.println(obj.toString());
		}
	}

}
