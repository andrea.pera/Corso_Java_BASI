package corso.lez20.spotifoo.model;

public abstract class User {
	
	protected int id;
	protected String username;
	protected String password;
	protected String name;
	protected String surname;
	
	public User() {}
	
	public User(String username, String password, String name, String surname) {
		this.setUsername(username);
		this.setPassword(password);
		this.setName(name);
		this.setSurname(surname);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	

}
