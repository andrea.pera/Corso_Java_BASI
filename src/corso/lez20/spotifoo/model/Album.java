package corso.lez20.spotifoo.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Album {
	
	private int id;
	private LocalDate data_rilascio;
	private Producer produttore;
	private double price;
	private List<Song> songs_list = new ArrayList<Song>();
	
	public Album() {}
	
	public Album(LocalDate data_rilascio, Producer produttore, double price) {
		this.setDataRilascio(data_rilascio);
		this.setProduttore(produttore);
		this.setPrice(price);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDataRilascio() {
		return data_rilascio;
	}

	public void setDataRilascio(LocalDate data_rilascio) {
		this.data_rilascio = data_rilascio;
	}

	public Producer getProduttore() {
		return produttore;
	}

	public void setProduttore(Producer produttore) {
		this.produttore = produttore;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public List<Song> getSongsList() {
		return this.songs_list;
	}
	
	public void addSong(Song song) {
		this.songs_list.add(song);
	}

	
}
