package corso.lez20.spotifoo.model;

import java.util.ArrayList;
import java.util.List;

public class RankingMostListenedAlbum implements Ranking {
	
	private List<Album> most_listened_albums = null;
	
	public RankingMostListenedAlbum() {
		this.most_listened_albums = new ArrayList<Album>();
	}
	
	public void addInRanking(Object album, int position) {
		this.most_listened_albums.add(position, (Album) album);
	}
	
	public void deleteFromRanking(Object album) {
		this.most_listened_albums.remove((Album) album);
	}
	
	public List<Object> getRanking() {
		return new ArrayList<Object>(this.most_listened_albums);
	}
	
}
