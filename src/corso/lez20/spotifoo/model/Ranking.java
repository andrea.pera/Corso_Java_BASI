package corso.lez20.spotifoo.model;

import java.util.List;

public interface Ranking {
	
	public void addInRanking(Object obj, int position);
	
	public void deleteFromRanking(Object obj); 
	
	public List<Object> getRanking();
}
