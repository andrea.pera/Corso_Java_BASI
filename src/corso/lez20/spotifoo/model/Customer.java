package corso.lez20.spotifoo.model;

public class Customer extends User {
	
	public Customer () {
		super();
	}
	
	public Customer(String username, String password, String name, String surname) {
		super(username, password, name, surname);
	}
	
	@Override
	public String toString() {
		return "ID: " + this.getId() + "\n" +
			   "Nome: " + this.getName() + "\n" + 
			   "Cognome: " + this.getSurname() + "\n" +
			   "Username: " + this.getUsername() + "\n" +
			   "Password: " + this.getPassword() + "\n";
	}
	
}
