package corso.lez20.spotifoo.model;

import java.util.List;

public class Playlist {
	
	private int id;
	private String name;
	private double durata;
	private List<Song> songs_list = null;
	
	public Playlist() {}
	
	public Playlist(String name, double durata) {
		this.setName(name);
		this.setDurata(durata);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDurata() {
		return durata;
	}

	public void setDurata(double durata) {
		this.durata = durata;
	}
	
	public List<Song> getSongsList() {
		return this.songs_list;
	}
	
	public void addSong(Song song) {
		this.songs_list.add(song);
	}
	
	public void removeSong(Song song) {
		this.songs_list.remove(song);
	}

}
