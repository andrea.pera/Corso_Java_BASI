package corso.lez20.spotifoo.model;

import java.util.ArrayList;
import java.util.List;

public class RankingMostListenedSong implements Ranking {
	
	private List<Song> most_listened_songs = null;
	
	public RankingMostListenedSong() {
		this.most_listened_songs = new ArrayList<Song>();
	}
	
	public void addInRanking(Object song, int position) {
		this.most_listened_songs.add(position, (Song) song);
	}
	
	public void deleteFromRanking(Object song) {
		this.most_listened_songs.remove((Song) song);
	}
	
	public List<Object> getRanking() {
		return new ArrayList<Object>(this.most_listened_songs);
	}
}
