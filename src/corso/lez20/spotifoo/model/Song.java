package corso.lez20.spotifoo.model;

public class Song {
	
	private int id;
	private String title;
	private double durata;
	private double price;
	private int bitrate;
	
	public Song() {}
	
	public Song(String title, double durata, double price, int bitrate) {
		this.setTitle(title);
		this.setDurata(durata);
		this.setPrice(price);
		this.setBitrate(bitrate);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getDurata() {
		return durata;
	}

	public void setDurata(double durata) {
		this.durata = durata;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getBitrate() {
		return bitrate;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}
	
	
	

}
