package corso.lez20.spotifoo.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import corso.lez20.spotifoo.model.Customer;

public class CustomerDAO {
	
	public static List<Customer> getAllCustomers() {
		
		List<Customer> customers_list = new ArrayList<Customer>();
		ResultSet result = null;
        Connection connection = null;
        Statement statement = null; 
        String query = "SELECT * " + 
        			   "FROM " + Customer.class.getSimpleName();
        
        try {           
            connection = JDBCMySQLConnection.getConnection();
            statement = connection.createStatement();
            result = statement.executeQuery(query);
             
            while(result.next()) {
            	Customer customer = new Customer(result.getString("username"),
            					  result.getString("pass_code"),
            					  result.getString("nome"),
            					  result.getString("cognome"));
            	customer.setId(result.getInt("id"));
            	customers_list.add(customer);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        } 
        finally {
            if (connection != null) {
                try {
                    connection.close();
                } 
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		
		return customers_list;
	}

}
