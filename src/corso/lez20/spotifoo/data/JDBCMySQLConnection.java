package corso.lez20.spotifoo.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;

public class JDBCMySQLConnection {

    private static JDBCMySQLConnection instance = new JDBCMySQLConnection();
    public static final String DEFAULT_URL = "jdbc:mysql://localhost/";
    public static final String DATABASE_NAME = "Spotifoo";
    public static final String PROPERTIES = "autoReconnect=true&useSSL=false";
    public static final String USER = "root";
    public static final String PASSWORD = "toortoor"; 
    public static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver"; 
     
    private JDBCMySQLConnection() {
        try {
            Class.forName(DRIVER_CLASS);
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
     
    private Connection createConnection() {
 
        Connection connection = null;
        Statement statement = null;

        try {
            connection = DriverManager.getConnection(DEFAULT_URL + DATABASE_NAME + "?" + PROPERTIES, USER, PASSWORD);

            statement = connection.createStatement();
            useDatabase(statement);
        } 
        catch (SQLException e) {
            System.out.println("ERROR: Unable to Connect to Database.");
        }
        return connection;
    }   
     
    public static Connection getConnection() {
        return instance.createConnection();
    }
    
    private void useDatabase(Statement statement) throws SQLException, SQLTimeoutException {
        String query = "USE " + DATABASE_NAME;
        statement.executeQuery(query);
    }
	
}
